import requests
import json
import mysql.connector

def uploadDatabase(obj, query1, query2, connection):
    text = dict(obj.json())["localidades"]
    for k in text:
        localidad = k
        provincia = k['provincia']['nombre']
        municipio = k['localidad_censal']['nombre']
        try:
            connection.execute(query1.format(k['provincia']['nombre'],k['provincia']['id']))
            print(query1.format(k['provincia']['nombre'],k['provincia']['id']))
        except Exception as e:
            pass
            #print(f"{e}")
        try:
            connection.execute(query2.format(k['localidad_censal']['nombre'],k['localidad_censal']['id'],k['provincia']['id']))
            print(query2.format(k['localidad_censal']['nombre'],k['localidad_censal']['id'],k['provincia']['id']))
        except Exception as e:
            pass
            #print(f"{e}")

def createConnection(hostName, userName, userPassword):
    try:
        connection = mysql.connector.connect(
            host = hostName,
            user = userName,
            passwd = userPassword
        )
        print("Connection Successful")
    except Exception as e:
        print(f"The error {e} ocurred")
    return connection

response = requests.get("https://infra.datos.gob.ar/catalog/modernizacion/dataset/7/distribution/7.5/download/localidades.json")


con = createConnection("127.0.0.1", "root", "root")
cursor = con.cursor()
cursor.execute("USE agenda;")
insertProvincia = "INSERT INTO provincias (nombre, idProvincia, idPaisFK) VALUES ('{}', {} ,0);"
insertLocalidad = "INSERT INTO localidades (nombre, idLocalidad, idProvinciaFK) VALUES ('{}', {},'{}');"

uploadDatabase(response, insertProvincia, insertLocalidad, cursor)
con.commit()
print(cursor.execute("SELECT * FROM provincias;"))
