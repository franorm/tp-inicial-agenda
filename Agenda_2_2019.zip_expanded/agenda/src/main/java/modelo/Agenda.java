package modelo;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dto.DireccionDTO;
import dto.LocalidadDTO;
import dto.PaisDTO;
import dto.PersonaDTO;
import dto.PersonasPorLocalidadesDTO;
import dto.ProvinciaDTO;
import dto.TipoDeContactoDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.DireccionDAO;
import persistencia.dao.interfaz.LocalidadDAO;
import persistencia.dao.interfaz.PaisDAO;
import persistencia.dao.interfaz.PersonaDAO;
import persistencia.dao.interfaz.ProvinciaDAO;
import persistencia.dao.interfaz.TipoDeContactoDAO;
import persistencia.conexion.Conexion;
import dto.GeneroMusicalDTO;
import persistencia.dao.interfaz.GeneroMusicalDAO;


public class Agenda 
{
	private PersonaDAO persona;
	private ProvinciaDAO provincias;
	private DireccionDAO direccion;
	private TipoDeContactoDAO tipoDeContacto;
	private LocalidadDAO localidad;
	private PaisDAO paises;
	private GeneroMusicalDAO generoMusical;
	
	public Agenda(DAOAbstractFactory metodo_persistencia)
	{
		this.persona = metodo_persistencia.createPersonaDAO();
		this.provincias = metodo_persistencia.createProvinciaDAO();
		this.direccion = metodo_persistencia.createDireccionDAO();
		this.tipoDeContacto = metodo_persistencia.createTipoDeContactoDAO();
		this.localidad = metodo_persistencia.createLocalidadDAO();
		this.paises = metodo_persistencia.createPaisDAO();
		this.generoMusical = metodo_persistencia.createGeneroMusicalDAO();
	}
	
	public void agregarPersona(PersonaDTO nuevaPersona)
	{
		this.persona.insert(nuevaPersona);
	}

	public void borrarPersona(PersonaDTO persona_a_eliminar) 
	{
		this.persona.delete(persona_a_eliminar);
	}
	
	public List<PersonaDTO> obtenerPersonas()
	{
		return this.persona.readAll();
	}
	
	public ArrayList<GeneroMusicalDTO> obtenerGenerosMusicales()
	{
		return this.generoMusical.readAll();
	}
	
	public void editarPersona(PersonaDTO persona_a_editar,int idPersona) 
	{
		this.persona.update(persona_a_editar, idPersona);
	}
	
	public void editarDireccion(DireccionDTO direccionAActualizar, int idDireccion) {
		this.direccion.update(direccionAActualizar, idDireccion);
	}
	
	public ArrayList<ProvinciaDTO> obtenerProvincias(){
		return this.provincias.readAll();
	}
	
	public HashMap<String, ProvinciaDTO> obtenerMapaProvincias(){
		ArrayList<ProvinciaDTO> provincias = this.obtenerProvincias();
		HashMap<String, ProvinciaDTO> ret = new HashMap<String, ProvinciaDTO>();
		for(ProvinciaDTO prov: provincias) ret.put(prov.getNombre(), prov);
		return ret;
	}

	public void agregarDireccion(DireccionDTO direccion) {
		this.direccion.insert(direccion);
		
	}

	private List<DireccionDTO> obtenerDirecciones() {
		return this.direccion.readAll();
	}
	
	public Map<Integer, DireccionDTO> obtenerMapaDirecciones(){
		HashMap<Integer, DireccionDTO> ret = new HashMap<Integer, DireccionDTO>();
		ArrayList<DireccionDTO> direcciones = (ArrayList<DireccionDTO>) this.obtenerDirecciones();
		for(DireccionDTO d: direcciones) {
			ret.put(d.getIdPersonaFK(), d);
		}
		return ret;
	}
	
	private List<TipoDeContactoDTO> obtenerEtiquetas(){
		return this.tipoDeContacto.readAll();
	}
	
	public HashMap<String, TipoDeContactoDTO> obtenerMapaEtiquetas(){
		ArrayList<TipoDeContactoDTO> etiquetas = (ArrayList<TipoDeContactoDTO>) this.obtenerEtiquetas();
		HashMap<String,TipoDeContactoDTO> ret = new HashMap<String,TipoDeContactoDTO>();
		for(TipoDeContactoDTO c: etiquetas) ret.put(c.getNombre(), c);
		return ret;
	}
	
	public ProvinciaDTO getProvinciaByName(String name) {
		return this.provincias.getProvinciaByName(name);
	}

	public void agregarLocalidad(LocalidadDTO nuevaLocalidad) {
		// TODO Auto-generated method stub
		this.localidad.insert(nuevaLocalidad);
	}
	
	public void eliminarLocalidad(LocalidadDTO localidadAEliminar) {
		this.localidad.delete(localidadAEliminar);
	}
	
	public void actualizarLocalidad(LocalidadDTO localidadAActualizar) {
		this.localidad.update(localidadAActualizar, localidadAActualizar.getIdLocalidad());
	}

	public PaisDTO getPaisByName(String nombrePais) {
		// TODO Auto-generated method stub
		return this.paises.getPaisByName(nombrePais);
	}
	
	public HashMap<String, ArrayList<ProvinciaDTO>> getMapPaises(){
		return this.paises.getMapPaises();
	}

	public void agregarProvincia(ProvinciaDTO nuevaProvincia) {
		// TODO Auto-generated method stub
		this.provincias.insert(nuevaProvincia);
	}

	public void eliminarProvincia(ProvinciaDTO provincia) {
		// TODO Auto-generated method stub
		this.provincias.delete(provincia);
	}

	public void actualizarProvincia(ProvinciaDTO provincia) {
		// TODO Auto-generated method stub
		this.provincias.update(provincia, provincia.getIdProvincia());
	}

	public void agregarPais(PaisDTO nuevoPais) {
		// TODO Auto-generated method stub
		this.paises.insert(nuevoPais);
	}

	public ArrayList<PaisDTO> getPaises() {
		// TODO Auto-generated method stub
		return this.paises.readAll();
	}

	public void eliminarPais(PaisDTO pais) {
		// TODO Auto-generated method stub
		this.paises.delete(pais);
	}

	public void actualizarPais(PaisDTO pais, int idPais) {
		// TODO Auto-generated method stub
		this.paises.update(pais, idPais);
	}
	
	
	
	public List<PersonasPorLocalidadesDTO> getDataSetReporteLocalidades(int pais) {
		// TODO Auto-generated method stub
		return this.direccion.reaDataSet(pais);
	}

	public GeneroMusicalDTO obtenerGeneroPorNombre(String generoMusical2) {
		// TODO Auto-generated method stub
		return this.generoMusical.readByName(generoMusical2);
	}
	
    public Connection getConexion() {
	return Conexion.getConexion().getSQLConexion();
    }

	public HashMap<String, ArrayList<LocalidadDTO>> obtenerLocalidadesPorProvincia() {
		// TODO Auto-generated method stub
		ArrayList<ProvinciaDTO> provincias = this.obtenerProvincias();
		HashMap<String, ArrayList<LocalidadDTO>> ret = new HashMap<String, ArrayList<LocalidadDTO>>();
		for(ProvinciaDTO provincia :provincias) ret.put(provincia.getNombre(), provincia.getLocalidades());
		return ret;
	}
	
	
}
