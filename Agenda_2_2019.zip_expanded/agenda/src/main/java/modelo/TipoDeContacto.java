package modelo;

public enum TipoDeContacto {
	Familiar, Amigo, Colega, Trabajo, Otros;
}
