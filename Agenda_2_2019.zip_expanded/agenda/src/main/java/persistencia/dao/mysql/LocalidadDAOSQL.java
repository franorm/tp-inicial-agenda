package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.LocalidadDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.LocalidadDAO;

public class LocalidadDAOSQL implements LocalidadDAO {

	private static final String insertLocalidad = "INSERT INTO localidades(nombre, idLocalidad, idProvinciaFK) VALUES (?,?,?)";
	private static final String deleteLocalidad = "DELETE FROM localidades WHERE idLocalidad = ?";
	private static final String updateLocalidad = "UPDATE localidades SET nombre = ? WHERE idLocalidad = ?";
	private static final String getAllLocalidad = "SELECT localidades.nombre, provincias.nombre, idLocalidad, idProvincia FROM localidades, provincias WHERE idProvinciaFK = idProvincia";
	
	@Override
	public boolean insert(LocalidadDTO localidad) {
		PreparedStatement statement;
		ResultSet resultSet;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(insertLocalidad);
			statement.setString(1, localidad.getNombre());
			statement.setInt(2, localidad.getIdLocalidad());
			statement.setInt(3, localidad.getIdProvincia());
			if(statement.executeUpdate()>0) {
				conexion.getSQLConexion().commit();
				return true;
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean delete(LocalidadDTO localidadAEliminar) {
		PreparedStatement statement;
		ResultSet resultSet;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(deleteLocalidad);
			statement.setInt(1, localidadAEliminar.getIdLocalidad());
			if(statement.executeUpdate() > 0) {
				conexion.getSQLConexion().commit();
				return true;
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean update(LocalidadDTO localidad, int idLocalidad) {
		// TODO Auto-generated method stub
		PreparedStatement statement;
		ResultSet resultSet;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(updateLocalidad);
			statement.setString(1, localidad.getNombre());
			statement.setInt(2, idLocalidad);
			if(statement.executeUpdate() > 0) {
			 conexion.getSQLConexion().commit();
			 return true;
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public List<LocalidadDTO> readAll() {
		PreparedStatement statement;
		ResultSet resultSet;
		Conexion conexion = Conexion.getConexion();
		ArrayList<LocalidadDTO> ret = new ArrayList<LocalidadDTO>();
		try {
			statement = conexion.getSQLConexion().prepareStatement(getAllLocalidad);
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				ret.add(getLocalidadDTO(resultSet));
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return ret;
	}
	
	private LocalidadDTO getLocalidadDTO(ResultSet resultSet) throws SQLException{
		String nombreLocalidad = resultSet.getString("localidades.nombre"), nombreProvincia = resultSet.getString("provincias.nombre");
		int idLocalidad = resultSet.getInt("localidades.idLocalidad"), idProvincia = resultSet.getInt("provincias.idProvincia");
		return new LocalidadDTO(nombreLocalidad, idLocalidad, nombreProvincia, idProvincia);
	}

}
