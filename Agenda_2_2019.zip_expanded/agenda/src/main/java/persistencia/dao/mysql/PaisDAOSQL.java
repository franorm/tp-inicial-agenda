package persistencia.dao.mysql;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import dto.LocalidadDTO;
import dto.PaisDTO;
import dto.ProvinciaDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.PaisDAO;

import persistencia.dao.mysql.ProvinciaDAOSQL;

public class PaisDAOSQL implements PaisDAO{
	
	private static final String insertPais = "INSERT INTO paises(nombre, idPais) VALUES (?,?)";
	private static final String deletePais = "DELETE FROM paises WHERE idPais = ?";
	private static final String updatePais = "UPDATE paises SET nombre = ? WHERE idPais = ?";
	private static final String readAllPaises = "SELECT * FROM paises";
	private static final String readAllProvinciasFromPais = "SELECT * FROM provincias WHERE idPaisFK = ?";
	private static final String readPaisByName = "SELECT * FROM paises WHERE nombre = ?";
	
	@Override
	public boolean insert(PaisDTO pais) {
		// TODO Auto-generated method stub
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(insertPais);
			statement.setString(1, pais.getNombre());
			statement.setInt(2, pais.getIdPais());
			if(statement.executeUpdate()>0) {
				conexion.getSQLConexion().commit();
				return true;
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean delete(PaisDTO paisAEliminar) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(deletePais);
			statement.setInt(1, paisAEliminar.getIdPais());
			if(statement.executeUpdate()>0) {
				conexion.getSQLConexion().commit();
				return true;
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean update(PaisDTO paisAActualizar, int idPais) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(updatePais);
			statement.setString(1, paisAActualizar.getNombre());
			statement.setInt(2, idPais);
			if(statement.executeUpdate()>0) {
				conexion.getSQLConexion().commit();
				return true;
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return false;
	}


	@Override
	public ArrayList<PaisDTO> readAll() {
		// TODO Auto-generated method stub
		ArrayList<PaisDTO> paises = new ArrayList<PaisDTO>();
		PreparedStatement statement;
		ResultSet resultSet;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readAllPaises);
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				paises.add(getPaisDTO(resultSet));
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return paises;
	}
	
	public HashMap<String, ArrayList<ProvinciaDTO>> getMapPaises(){
		ArrayList<PaisDTO> paises = this.readAll();
		HashMap<String, ArrayList<ProvinciaDTO>> ret = new HashMap<String, ArrayList<ProvinciaDTO>>();
		for(PaisDTO pais :paises) ret.put(pais.getNombre(), pais.getProvincias());
		return ret;
	}
	
	private ArrayList<ProvinciaDTO> readAllProvincesById(int idPais) {
		// TODO Auto-generated method stub
		ArrayList<ProvinciaDTO> provinciasPorPais = new ArrayList<ProvinciaDTO>();
		PreparedStatement statement;
		ResultSet resultSet;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readAllProvinciasFromPais);
			statement.setInt(1, idPais);
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				provinciasPorPais.add(this.getProvinciaDTO(resultSet));
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return provinciasPorPais;
	}
	
	public PaisDTO getPaisByName(String nombrePais) {
		PreparedStatement statement;
		ResultSet resultSet;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readPaisByName);
			statement.setString(1, nombrePais);
			resultSet = statement.executeQuery();
			if(resultSet.next()) {
				return this.getPaisDTO(resultSet);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private PaisDTO getPaisDTO(ResultSet resultSet) throws SQLException {
		String nombrePais = resultSet.getString("nombre");
		int idPais = resultSet.getInt("idPais");
		ArrayList<ProvinciaDTO> provinciasPorPais = this.readAllProvincesById(idPais);
		return new PaisDTO(idPais, nombrePais, provinciasPorPais);
	}

	private ProvinciaDTO getProvinciaDTO(ResultSet resultSet) throws SQLException
	{
		String nombre = resultSet.getString("nombre");
		ProvinciaDAOSQL provinciaSQL = new ProvinciaDAOSQL();
		ArrayList<LocalidadDTO> localidades = provinciaSQL.readAllLocalities(nombre);
		int id = resultSet.getInt("idProvincia");
		int idPais = resultSet.getInt("idPaisFK");
		return new ProvinciaDTO(nombre, localidades, id, idPais);
	}
	
}
