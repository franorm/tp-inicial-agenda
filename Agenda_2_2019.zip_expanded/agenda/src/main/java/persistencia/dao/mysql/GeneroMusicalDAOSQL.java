package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.LocalidadDTO;
import dto.PersonaDTO;
import dto.ProvinciaDTO;
import dto.GeneroMusicalDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.ProvinciaDAO;
import persistencia.dao.interfaz.GeneroMusicalDAO;

public class GeneroMusicalDAOSQL implements GeneroMusicalDAO {
	
	private static final String readAllGenerosMusicales = "SELECT * FROM generosMusicales;";
	private static final String insertGenerosMusicales = "INSERT INTO agenda.generosMusicales values (?);";
	private static final String getGeneroMusicalByNombre = "SELECT * FROM generosMusicales WHERE nombre = ?";
	private static final String getGeneroMusicalById = "SELECT * FROM generosMusicales WHERE idGeneroMusical = ?";

	@Override
	public boolean insertGeneroMusical(GeneroMusicalDTO generoMusical, int index) {
		PreparedStatement statementGeneroMusical;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try
		{
			statementGeneroMusical = conexion.prepareStatement(insertGenerosMusicales);			
			
			statementGeneroMusical.setString(1, generoMusical.getNombre());
			
			if(statementGeneroMusical.executeUpdate() > 0 )
			{
				conexion.commit();
				isInsertExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		return isInsertExitoso;
	}

	@Override
	public boolean delete(GeneroMusicalDTO generoMusical_a_eliminar) {
		return false;
	}

	@Override
	public boolean update(GeneroMusicalDTO generoMusical) {
		return false;
	}

	@Override
	public ArrayList<GeneroMusicalDTO> readAll() {
			PreparedStatement statement;
			ResultSet resultSet; //Guarda el resultado de la query
			ArrayList<GeneroMusicalDTO> generosMusicales = new ArrayList<GeneroMusicalDTO>();
			Conexion conexion = Conexion.getConexion();
			try 
			{
				statement = conexion.getSQLConexion().prepareStatement(readAllGenerosMusicales);
				resultSet = statement.executeQuery();
				while(resultSet.next())
				{
					generosMusicales.add(getGeneroMusicalDTO(resultSet));
				}
			} 
			catch (SQLException e) 
			{
				e.printStackTrace();
			}
			return generosMusicales;
		
	}
	
	private GeneroMusicalDTO getGeneroMusicalDTO(ResultSet resultSet) throws SQLException
	{
		String nombre = resultSet.getString("nombre");
		int idGeneroMusical = resultSet.getInt("idGeneroMusical");
		return new GeneroMusicalDTO(idGeneroMusical,nombre);
	}

	@Override
	public GeneroMusicalDTO readByName(String name) {
		PreparedStatement statement;
		ResultSet resultSet;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(getGeneroMusicalByNombre);
			statement.setString(1, name);
			resultSet = statement.executeQuery();
			if(resultSet.next()) {
				return this.getGeneroMusicalDTO(resultSet);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public GeneroMusicalDTO readById(int id) {
		PreparedStatement statement;
		ResultSet resultSet;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(getGeneroMusicalById);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			if(resultSet.next()) {
				return this.getGeneroMusicalDTO(resultSet);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}