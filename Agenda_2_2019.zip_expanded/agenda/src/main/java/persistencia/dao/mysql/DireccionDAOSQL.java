package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.DireccionDTO;
import dto.PersonaDTO;
import dto.PersonasPorLocalidadesDTO;
import dto.ProvinciaDTO;
import dto.TipoDeContactoDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.DireccionDAO;
import persistencia.dao.mysql.ProvinciaDAOSQL;

public class DireccionDAOSQL implements DireccionDAO {
	
	public static final String insertDireccion = "INSERT INTO direcciones(calle, localidad, departamento, altura, piso, idDireccion, idPersonaFK, idProvinciaFK) VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String deleteDireccion = "DELETE FROM direcciones WHERE idDireccion = ?";
	private static final String updateDireccion = "UPDATE direcciones SET  calle= ?, localidad= ?, idProvinciaFK= ?, altura = ?, piso = ?, departamento = ? WHERE idDireccion = ?";
	private static final String readallDireccion = "SELECT * FROM direcciones";
	private static final String readADireccion = "SELECT * FROM direcciones WHERE idPersonaFk = ?";
	
	
	private static final String datasetPersonas_x_Localidad = "select localidad , count(l.idPersonaFk) as cantidad from direcciones l inner join provincias p  on l.idProvinciaFK = p.idProvincia  where p.idPaisFK = ? group by localidad order by count(l.idPersonaFk) desc";
	
	@Override
	public boolean insert(DireccionDTO direccionAAgregar) {
		
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try
		{
			statement = conexion.prepareStatement(insertDireccion);
			statement.setString(1, direccionAAgregar.getCalle());
			statement.setString(2, direccionAAgregar.getLocalidad());
			statement.setString(3, direccionAAgregar.getDepartamento());
			statement.setInt(4, direccionAAgregar.getAltura());
			statement.setInt(5, direccionAAgregar.getPiso());
			statement.setInt(6, direccionAAgregar.hashCode());
			statement.setInt(7, direccionAAgregar.getIdPersonaFK());
			statement.setInt(8, direccionAAgregar.getIdProvinciaFK());
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isInsertExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		System.out.println(isInsertExitoso);
		return isInsertExitoso;
	}

	@Override
	public boolean delete(DireccionDTO direccionAEliminar) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(deleteDireccion);
			statement.setString(1, Integer.toString(direccionAEliminar.getIdDireccion()));
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isdeleteExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}
	
	@Override
	public boolean update(DireccionDTO direccion, int idDireccion) {
			PreparedStatement statement;
			Connection conexion = Conexion.getConexion().getSQLConexion();
			boolean isupdateExitoso = false;
			try 
			{
				statement = conexion.prepareStatement(updateDireccion);
				statement.setString(1, direccion.getCalle());
				statement.setString(2, direccion.getLocalidad());
				statement.setInt(3, direccion.getIdProvinciaFK());
				statement.setInt(4, direccion.getAltura());
				statement.setInt(5, direccion.getPiso());
				statement.setString(6, direccion.getDepartamento());
				statement.setInt(7, idDireccion);
				
				System.out.println(statement.toString());
				
				if(statement.executeUpdate() > 0)
				{
					conexion.commit();
					isupdateExitoso = true;
				}
			} 
			catch (SQLException e) 
			{
				e.printStackTrace();
			}
			return isupdateExitoso;
	}
	
//	private static final String updateDireccion = "UPDATE direcciones SET  calle= ?, localidad= ?, provincia= ?, altura = ?, piso = ?, departamento = ?, idPersonaFk = ? WHERE idDireccion = ?";

	
	@Override
	public List<DireccionDTO> readAll() {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<DireccionDTO> direcciones = new ArrayList<DireccionDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readallDireccion);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				direcciones.add(getDireccionDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return direcciones;
	}
	
	private DireccionDTO getDireccionDTO(ResultSet resultSet) throws SQLException {
		String calle = resultSet.getString("calle");
		String localidad = resultSet.getString("localidad");
		String departamento = resultSet.getString("departamento");
		int altura = resultSet.getInt("altura"), piso = resultSet.getInt("piso"), idPersonaFK = resultSet.getInt("idPersonaFK"), idDireccion = resultSet.getInt("idDireccion"), idProvinciaFK = resultSet.getInt("idProvinciaFK");
		ProvinciaDAOSQL provincias = new ProvinciaDAOSQL();
		ProvinciaDTO provincia = provincias.getProvinciaById(idProvinciaFK);
		DireccionDTO direccion = new DireccionDTO(calle, localidad, provincia.getNombre(), departamento, altura, piso, idPersonaFK, idProvinciaFK);
		direccion.setIdDireccion(idDireccion);
		return direccion;
	}
	//String calle, String localidad, String provincia, String departamento, int altura, int piso, int idPersonaFK

	@Override
	public DireccionDTO readAdireccion(int idPersona) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		DireccionDTO direccion = null;
		try {
			statement = conexion.getSQLConexion().prepareStatement(readADireccion);
			int id = idPersona;
			statement.setInt(1, id);
			ResultSet resultSet = statement.executeQuery();
			while(resultSet.next()) {
				direccion = getDireccionDTO(resultSet);
			}
			}catch(SQLException e) {
			e.printStackTrace();
			}
		return direccion;
	}
	
	private PersonasPorLocalidadesDTO getPersonasPorLocalidadesDTO(ResultSet resultSet) throws SQLException
	{
		int cantidad = resultSet.getInt("cantidad");
		String localidad = resultSet.getString("localidad");
		return new PersonasPorLocalidadesDTO(localidad, cantidad);
	}
	
	public List<PersonasPorLocalidadesDTO> reaDataSet(int pais) {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<PersonasPorLocalidadesDTO> dataset = new ArrayList<PersonasPorLocalidadesDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(datasetPersonas_x_Localidad);
			statement.setInt(1, pais);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				dataset.add(getPersonasPorLocalidadesDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return dataset;
	}
}
