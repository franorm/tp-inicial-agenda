package persistencia.dao.mysql;

import java.sql.Date;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.PersonaDAO;
import dto.GeneroMusicalDTO;
import dto.PersonaDTO;
import dto.TipoDeContactoDTO;

public class PersonaDAOSQL implements PersonaDAO
{
	private static final String insert = "INSERT INTO personas(idPersona, nombre, telefono, Email, FechaCumpleanios, idEtiqueta, idGeneroMusical) VALUES(?, ?, ?, ?, ?, ?, ?)";
	private static final String delete = "DELETE FROM personas WHERE idPersona = ?";
	private static final String update = "UPDATE personas SET  Nombre= ?, Telefono= ?,Email= ?, FechaCumpleanios = ?, idEtiqueta = ? WHERE idPersona = ?";
	private static final String readall = "SELECT * FROM personas";
	private static final String readATipoDeContactoById = "SELECT * FROM tiposDeContacto WHERE idTipoDeContacto = ?";
		
	public boolean insert(PersonaDTO persona)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try
		{
			statement = conexion.prepareStatement(insert);
			statement.setInt(1, persona.getIdPersona());
			statement.setString(2, persona.getNombre());
			statement.setString(3, persona.getTelefono());
			statement.setString(4, persona.getEmail());
			statement.setDate(5, new Date( persona.getFechaCumpleanios().getTime()));
			statement.setInt(6, persona.getEtiqueta().getIdTipoDeContacto());
			statement.setInt(7, persona.getGeneroFavorito().getId());
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isInsertExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}
	
	public boolean delete(PersonaDTO persona_a_eliminar)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(delete);
			statement.setString(1, Integer.toString(persona_a_eliminar.getIdPersona()));
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isdeleteExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}
	
	public boolean update(PersonaDTO persona, int idPersona)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(update);
			statement.setString(1, persona.getNombre());
			statement.setString(2, persona.getTelefono());
			statement.setString(3, persona.getEmail());
			statement.setDate(4, new Date( persona.getFechaCumpleanios().getTime()));
			statement.setInt(5, persona.getEtiqueta().getIdTipoDeContacto());
			statement.setInt(6, idPersona);
			
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isdeleteExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}
	
	public List<PersonaDTO> readAll()
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<PersonaDTO> personas = new ArrayList<PersonaDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				personas.add(getPersonaDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return personas;
	}
	
	private TipoDeContactoDTO getTipoDeContacto(int id) {
		PreparedStatement statement;
		ResultSet resultSet;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readATipoDeContactoById);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			if(resultSet.next()) {
				return this.getTipoDeContactoDTO(resultSet);
			}
		}catch(SQLException e) {
			
		}return null;
	}
	
	private TipoDeContactoDTO getTipoDeContactoDTO(ResultSet resultSet) throws SQLException{
		int id = resultSet.getInt("idTipoDeContacto");
		String nombre = resultSet.getString("nombre");
		return new TipoDeContactoDTO(nombre, id);
	}
	
	private PersonaDTO getPersonaDTO(ResultSet resultSet) throws SQLException
	{
		int id = resultSet.getInt("idPersona");
		String nombre = resultSet.getString("Nombre");
		String tel = resultSet.getString("Telefono");
		String email = resultSet.getString("Email");
		Date fechaCumpleanios = resultSet.getDate("FechaCumpleanios");
		int idGeneroMusical = resultSet.getInt("idGeneroMusical");
		int idEtiqueta = resultSet.getInt("idEtiqueta");
		TipoDeContactoDTO etiqueta = this.getTipoDeContacto(idEtiqueta);
		System.out.println(etiqueta.toString());
		GeneroMusicalDAOSQL generos = new GeneroMusicalDAOSQL();
		GeneroMusicalDTO favorito = generos.readById(idGeneroMusical);
		return new PersonaDTO(id, nombre, tel,email, fechaCumpleanios, etiqueta, favorito);
	}
}
