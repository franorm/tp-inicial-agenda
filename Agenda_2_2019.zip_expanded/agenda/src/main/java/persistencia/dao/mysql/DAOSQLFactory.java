/**
 * 
 */
package persistencia.dao.mysql;

import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.DireccionDAO;
import persistencia.dao.interfaz.LocalidadDAO;
import persistencia.dao.interfaz.PaisDAO;
import persistencia.dao.interfaz.PersonaDAO;
import persistencia.dao.interfaz.ProvinciaDAO;
import persistencia.dao.interfaz.TipoDeContactoDAO;
import persistencia.dao.interfaz.GeneroMusicalDAO;

public class DAOSQLFactory implements DAOAbstractFactory 
{
	/* (non-Javadoc)
	 * @see persistencia.dao.interfaz.DAOAbstractFactory#createPersonaDAO()
	 */
	public PersonaDAO createPersonaDAO() 
	{
		return new PersonaDAOSQL();
	}
	
	public DireccionDAO createDireccionDAO() {
		return new DireccionDAOSQL();
	}
	
	public ProvinciaDAO createProvinciaDAO() {
		return new ProvinciaDAOSQL();
	}
	
	public LocalidadDAO createLocalidadDAO() {
		return new LocalidadDAOSQL();
	}

	public PaisDAO createPaisDAO() {
		return new PaisDAOSQL();
	}
	
	public TipoDeContactoDAO createTipoDeContactoDAO() {
		return new TipoDeContactoDAOSQL();
	}
	
	public GeneroMusicalDAO createGeneroMusicalDAO() {
		return new GeneroMusicalDAOSQL();
	}
	
}
