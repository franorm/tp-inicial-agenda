package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.TipoDeContactoDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.TipoDeContactoDAO;

public class TipoDeContactoDAOSQL implements TipoDeContactoDAO {
	
	private static final String readAllTipoDeContacto = "SELECT * FROM tiposDeContacto";

	@Override
	public boolean insert(TipoDeContactoDTO localidad) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(TipoDeContactoDTO localidadAEliminar) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(TipoDeContactoDTO localidad, int idLocalidad) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<TipoDeContactoDTO> readAll() {
		ArrayList<TipoDeContactoDTO> ret = new ArrayList<TipoDeContactoDTO>();
		PreparedStatement statement;
		ResultSet resultSet;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readAllTipoDeContacto);
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				ret.add(this.getTipoDeContactoDTO(resultSet));
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return ret;
	}
	
	private TipoDeContactoDTO getTipoDeContactoDTO(ResultSet resultSet) throws SQLException{
		String nombre = resultSet.getString("nombre");
		int id = resultSet.getInt("idTipoDeContacto");
		return new TipoDeContactoDTO(nombre, id);
	}

}
