package persistencia.dao.mysql;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.LocalidadDTO;
import dto.PersonaDTO;
import dto.ProvinciaDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.ProvinciaDAO;

public class ProvinciaDAOSQL implements ProvinciaDAO {
	
	private static final String readAllProvinces = "SELECT * FROM provincias;";
	private static final String readAllLocalities = "SELECT localidades.nombre, localidades.idLocalidad, provincias.nombre, provincias.idProvincia FROM localidades, provincias WHERE (provincias.nombre = ? AND provincias.idProvincia = localidades.idProvinciaFK);";
	private static final String updateAProvince = "UPDATE provincias SET nombre = ? WHERE idProvincia = ?";
	private static final String deleteAProvince = "DELETE FROM provincias WHERE idProvincia = ?";
	private static final String insertAProvince = "INSERT INTO provincias(nombre, idProvincia, idPaisFK) VALUES (?,?,?)";
	private static final String getAProvinceByName = "SELECT * FROM provincias WHERE nombre = ?";
	private static final String getAProvinceById = "SELECT * FROM provincias WHERE idProvincia = ?";
	
	@Override
	public boolean insert(ProvinciaDTO provincia) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(insertAProvince);
			statement.setString(1, provincia.getNombre());
			statement.setInt(2, provincia.getIdProvincia());
			statement.setInt(3, provincia.getIdPais());
			if(statement.executeUpdate() > 0) {
				conexion.getSQLConexion().commit();
				return true;
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}return false;
	}

	@Override
	public boolean delete(ProvinciaDTO provincia_a_eliminar) {
		PreparedStatement statement;
		ResultSet resultSet;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(deleteAProvince);
			statement.setInt(1, provincia_a_eliminar.getIdProvincia());
			if(statement.executeUpdate() > 0) {
				conexion.getSQLConexion().commit();
				return true;
			}
			}catch(SQLException e) {
				e.printStackTrace();
			}
		
			return false;
		}

	@Override
	public boolean update(ProvinciaDTO provinciaAActualizar, int idProvincia) {
		// TODO Auto-generated method stub
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(updateAProvince);
			statement.setString(1, provinciaAActualizar.getNombre());
			statement.setInt(2, idProvincia);
			if(statement.executeUpdate() > 0) {
				conexion.getSQLConexion().commit();
				return true;
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public ArrayList<ProvinciaDTO> readAll() {
			PreparedStatement statement;
			ResultSet resultSet; //Guarda el resultado de la query
			ArrayList<ProvinciaDTO> provincias = new ArrayList<ProvinciaDTO>();
			Conexion conexion = Conexion.getConexion();
			try 
			{
				statement = conexion.getSQLConexion().prepareStatement(readAllProvinces);
				resultSet = statement.executeQuery();
				while(resultSet.next())
				{
					provincias.add(getProvinciaDTO(resultSet));
				}
			} 
			catch (SQLException e) 
			{
				e.printStackTrace();
			}
			return provincias;
		
	}
	
	public ArrayList<LocalidadDTO> readAllLocalities(String province) {
		PreparedStatement statement;
		ResultSet resultSet;
		ArrayList<LocalidadDTO> localidades = new ArrayList<LocalidadDTO>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readAllLocalities);
			statement.setString(1, province);
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				localidades.add(getLocalidadDTO(resultSet));
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return localidades;
	}
	
	private LocalidadDTO getLocalidadDTO(ResultSet resultSet) throws SQLException{
		String nombre = resultSet.getString("localidades.nombre"), provincia = resultSet.getString("provincias.nombre");
		int idLocalidad = resultSet.getInt("localidades.idLocalidad"), provinciaIdFK = resultSet.getInt("provincias.idProvincia");
		return new LocalidadDTO(nombre,idLocalidad, provincia, provinciaIdFK);
	}
	
	private ProvinciaDTO getProvinciaDTO(ResultSet resultSet) throws SQLException
	{
		String nombre = resultSet.getString("nombre");
		ArrayList<LocalidadDTO> localidades = readAllLocalities(nombre);
		int id = resultSet.getInt("idProvincia");
		int idPais = resultSet.getInt("idPaisFK");
		return new ProvinciaDTO(nombre, localidades, id, idPais);
	}
	
	public static void main(String args[]) {
		ProvinciaDAOSQL prueba = new ProvinciaDAOSQL();
		ArrayList<ProvinciaDTO> p = prueba.readAll();
		for(ProvinciaDTO pr :p) System.out.println(pr.toString());
	}

	@Override
	public ProvinciaDTO getProvinciaByName(String name) {
		PreparedStatement statement;
		ResultSet resultSet;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(getAProvinceByName);
			statement.setString(1, name);
			resultSet = statement.executeQuery();
			if(resultSet.next()) {
				return this.getProvinciaDTO(resultSet);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public ProvinciaDTO getProvinciaById(int id) {
		PreparedStatement statement;
		ResultSet resultSet;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(getAProvinceById);
			statement.setInt(1,id);
			resultSet = statement.executeQuery();
			if(resultSet.next()) {
				return this.getProvinciaDTO(resultSet);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
