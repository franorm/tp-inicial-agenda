package persistencia.dao.interfaz;

import java.util.ArrayList;
import java.util.List;

import dto.GeneroMusicalDTO;

public interface GeneroMusicalDAO {

	public boolean insertGeneroMusical(GeneroMusicalDTO generoMusical,int index);

	public boolean delete(GeneroMusicalDTO generoMusical_a_eliminar);
	
	public boolean update(GeneroMusicalDTO generoMusical);
	
	public ArrayList<GeneroMusicalDTO> readAll();
	
	public GeneroMusicalDTO readByName(String name);
	
	public GeneroMusicalDTO readById(int id);
}