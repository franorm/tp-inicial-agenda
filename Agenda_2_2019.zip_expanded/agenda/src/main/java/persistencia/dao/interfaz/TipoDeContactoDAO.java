package persistencia.dao.interfaz;

import java.util.List;

import dto.TipoDeContactoDTO;

public interface TipoDeContactoDAO {
	
	public boolean insert(TipoDeContactoDTO localidad);

	public boolean delete(TipoDeContactoDTO localidadAEliminar);
	
	public boolean update(TipoDeContactoDTO localidad, int idLocalidad);
	
	public List<TipoDeContactoDTO> readAll();
}
