package persistencia.dao.interfaz;

import java.util.ArrayList;
import java.util.HashMap;

import dto.PaisDTO;
import dto.ProvinciaDTO;

public interface PaisDAO {

	public boolean insert(PaisDTO pais);

	public boolean delete(PaisDTO paisAEliminar);
	
	public boolean update(PaisDTO paisAActualizar, int idPais);
	
	public ArrayList<PaisDTO> readAll();
	
	public PaisDTO getPaisByName(String nombrePais);
	
	public HashMap<String, ArrayList<ProvinciaDTO>> getMapPaises();
}
