package persistencia.dao.interfaz;

import java.util.ArrayList;
import java.util.List;

import dto.ProvinciaDTO;

public interface ProvinciaDAO {

	public boolean insert(ProvinciaDTO provincia);

	public boolean delete(ProvinciaDTO provincia_a_eliminar);
	
	public boolean update(ProvinciaDTO provinciaAActualizar, int idProvincia);
	
	public ProvinciaDTO getProvinciaByName(String name);
	
	public ArrayList<ProvinciaDTO> readAll();
	
}
