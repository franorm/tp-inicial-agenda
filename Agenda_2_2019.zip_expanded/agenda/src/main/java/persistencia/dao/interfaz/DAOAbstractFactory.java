package persistencia.dao.interfaz;


public interface DAOAbstractFactory 
{
	public PersonaDAO createPersonaDAO();
	public DireccionDAO createDireccionDAO();
	public ProvinciaDAO createProvinciaDAO();
	public LocalidadDAO createLocalidadDAO();
	public PaisDAO createPaisDAO();
	public TipoDeContactoDAO createTipoDeContactoDAO();
	public GeneroMusicalDAO createGeneroMusicalDAO();
}
