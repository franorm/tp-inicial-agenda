package persistencia.dao.interfaz;

import java.util.List;

import dto.DireccionDTO;
import dto.PersonaDTO;
import dto.PersonasPorLocalidadesDTO;

public interface DireccionDAO {
	
	public boolean insert(DireccionDTO direccionAAgregar);
	
	public boolean delete(DireccionDTO direccionAEliminar);
	
	public boolean update(DireccionDTO direccionAActualizar, int idDireccion);
	
	public List<DireccionDTO> readAll();
	
	public List<PersonasPorLocalidadesDTO> reaDataSet(int pais);
	
	public DireccionDTO readAdireccion(int idPersona);

}
