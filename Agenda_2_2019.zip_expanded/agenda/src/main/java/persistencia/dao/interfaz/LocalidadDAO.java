package persistencia.dao.interfaz;

import java.util.List;

import dto.LocalidadDTO;

public interface LocalidadDAO {
	
	public boolean insert(LocalidadDTO localidad);

	public boolean delete(LocalidadDTO localidadAEliminar);
	
	public boolean update(LocalidadDTO localidad, int idLocalidad);
	
	public List<LocalidadDTO> readAll();

}
