package presentacion.vista;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import dto.LocalidadDTO;
import dto.PersonaDTO;
import dto.ProvinciaDTO;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;

public class VentanaProvincia extends JFrame{
	private static final long serialVersionUID = 1L;
	private JButton btnGuardar;
	private JButton btnAgregar;
	private JButton btnEliminar;
	private JButton btnEditar;
	private JPanel contentPane;
	private JTextField txtProvincia;
	private JComboBox comboBoxPais;
	private String[] columnas = new String[] {"Provincia"};
	private DefaultTableModel modelo = new DefaultTableModel(null, columnas);
	private HashMap<String,ArrayList<ProvinciaDTO>> diccionarioProvincias;
	
	private static VentanaProvincia INSTANCE;
	private JTable table;


	public static VentanaProvincia getInstance(HashMap<String, ArrayList<ProvinciaDTO>> diccionarioProvincia)
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaProvincia(diccionarioProvincia); 	
		}
		return INSTANCE;
	}

	private VentanaProvincia(HashMap<String, ArrayList<ProvinciaDTO>> diccionarioProvincias) 
	{
		super();
		this.diccionarioProvincias = diccionarioProvincias;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 596, 528);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 571, 481);
		contentPane.add(panel);
		panel.setLayout(null);
		// LABELS
		JLabel lblPais = new JLabel("Pais");
		lblPais.setBounds(10, 33, 113, 14);
		panel.add(lblPais);
		
		JLabel lblProvincia = new JLabel("Provincia");
		lblProvincia.setBounds(10, 69, 113, 14);
		panel.add(lblProvincia);
		
		this.txtProvincia = new JTextField();
		txtProvincia.setBounds(294, 66, 164, 20);
		panel.add(txtProvincia);
		txtProvincia.setColumns(10);
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(468, 440, 89, 30);
		panel.add(btnGuardar);
		
		comboBoxPais = new JComboBox();
		comboBoxPais.setBounds(294, 29, 164, 22);
		comboBoxPais.addItem("");
		Set<String> provincias = this.diccionarioProvincias.keySet();
		for(String provincia: provincias) {
			comboBoxPais.addItem(provincia);
			comboBoxPais.addItemListener(new ItemListener() {

				@Override
				public void itemStateChanged(ItemEvent event) {
					if(event.getStateChange() == ItemEvent.SELECTED) {
						Object item = event.getItem();
						llenarTabla(item.toString());
						}
					}
				}
			);
			
		}
		panel.add(comboBoxPais);
		
		btnEliminar = new JButton("Eliminar");
		btnEliminar.setBounds(468, 406, 89, 23);
		panel.add(btnEliminar);
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(472, 65, 89, 23);
		panel.add(btnAgregar);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 119, 448, 351);
		panel.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		table.setModel(this.modelo);
		
		btnEditar = new JButton("Editar");
		btnEditar.setBounds(468, 372, 89, 23);
		panel.add(btnEditar);
		
		}
	
	
	public HashMap<String, ArrayList<ProvinciaDTO>> getDiccionarioProvincias() {
		return diccionarioProvincias;
	}

	public void setDiccionarioProvincias(HashMap<String, ArrayList<ProvinciaDTO>> diccionarioProvincias) {
		this.diccionarioProvincias = diccionarioProvincias;
	}

	public void mostrarVentana()
	{
		//this.reiniciarFormulario();
		this.setVisible(true);
	}
	
	public JButton getBtnAgregar() {
		return this.btnAgregar;
	}
	
	public JButton getBtnEliminar() {
		return this.btnEliminar;
	}
	
	public JButton getBtnEditar() {
		return this.btnEditar;
	}
	
	public JComboBox getProvinciaComboBox() {
		return this.comboBoxPais;
	}
	
	public JTextField getProvinciaInput() {
		return this.txtProvincia;
	}
	
	public JTable getTableProvincia() {
		return this.table;
	}
	
	public void llenarTabla(String pais) {
		ArrayList<ProvinciaDTO> provincias = this.diccionarioProvincias.get(pais);
		this.modelo.setRowCount(0);
		this.modelo.setColumnCount(0);
		this.modelo.setColumnIdentifiers(this.columnas);
		if(!provincias.isEmpty()) {
			for(ProvinciaDTO p: provincias) {
				Object[] filaProvincia = {p.getNombre()};
				this.modelo.addRow(filaProvincia);
			}
		}
	}
}
