package presentacion.vista;

import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import dto.PersonaDTO;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import persistencia.conexion.Conexion;

public class Vista
{
	private JFrame frame;
	private JTable tablaPersonas;
	private JButton btnAgregar;
	private JButton btnEditar;
	private JButton btnBorrar;
	private JButton btnReporte;
	private JButton btnReporteLocal;
	private JButton btnVer;
	
	private JButton btnConfiguracion;
	private DefaultTableModel modelPersonas;
	private  String[] nombreColumnas = {"Nombre y apellido","Telefono","Email","Fecha Cumpleaños","Tipo De Contacto"};

	public Vista() 
	{
		super();
		initialize();
	}


	private void initialize() 
	{
		

		  
		  
		frame = new JFrame();
		frame.setBounds(100, 100, 1000, 440);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 1000, 440);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JScrollPane spPersonas = new JScrollPane();
		spPersonas.setBounds(10, 11, 648, 324);
		panel.add(spPersonas);
		
		modelPersonas = new DefaultTableModel(null,nombreColumnas);
		tablaPersonas = new JTable(modelPersonas);
		
		tablaPersonas.getColumnModel().getColumn(0).setPreferredWidth(103);
		tablaPersonas.getColumnModel().getColumn(0).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(1).setPreferredWidth(100);
		tablaPersonas.getColumnModel().getColumn(1).setResizable(false);
		
		spPersonas.setViewportView(tablaPersonas);
		
		btnAgregar = new JButton("");
		btnAgregar.setBounds(10, 346, 89, 40);
		ImageIcon iconAgregar = new ImageIcon("imagenes/add-user.png");

		btnAgregar.setIcon(iconAgregar);

		panel.add(btnAgregar);

		  
		btnEditar = new JButton("");
		btnEditar.setBounds(109, 346, 89, 40);
		panel.add(btnEditar);
		ImageIcon iconEditar = new ImageIcon("imagenes/edit.png");
		btnEditar.setIcon(iconEditar);
		
		btnBorrar = new JButton("");
		btnBorrar.setBounds(208, 346, 89, 40);
		ImageIcon iconBorrar = new ImageIcon("imagenes/delete.png");
		btnBorrar.setIcon(iconBorrar);
		panel.add(btnBorrar);
		
		
		btnReporte = new JButton("Reporte Contactos");
		btnReporte.setBounds(700, 10, 200, 23);
		panel.add(btnReporte);
		
		btnReporteLocal = new JButton("Reporte Localidades");
		btnReporteLocal.setBounds(700, 40, 200, 23);
		panel.add(btnReporteLocal);
		
		btnVer = new JButton("");
		btnVer.setBounds(569, 346, 89, 40);
		ImageIcon iconVer = new ImageIcon("imagenes/call.png");
		btnVer.setIcon(iconVer);
		panel.add(btnVer);
		
		btnConfiguracion = new JButton("");
		btnConfiguracion.setBounds(441, 346, 89, 40);
		ImageIcon iconConfiguracion = new ImageIcon("imagenes/config.png");
		btnConfiguracion.setIcon(iconConfiguracion);
		panel.add(btnConfiguracion);
	}
	
	public void show()
	{
		this.frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.frame.addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
		        int confirm = JOptionPane.showOptionDialog(
		             null, "¿Estás seguro que quieres salir de la Agenda?", 
		             "Confirmación", JOptionPane.YES_NO_OPTION,
		             JOptionPane.QUESTION_MESSAGE, null, null, null);
		        if (confirm == 0) {
		        	Conexion.getConexion().cerrarConexion();
		           System.exit(0);
		        }
		    }
		});
		this.frame.setVisible(true);
	}
	
	public JButton getBtnAgregar() 
	{
		return btnAgregar;
	}
	
	public JButton getBtnEditar() 
	{
		return btnEditar;
	}

	public JButton getBtnBorrar() 
	{
		return btnBorrar;
	}
	
	public JButton getBtnReporte() 
	{
		return btnReporte;
	}
	
	public JButton getBtnVer() {
		return this.btnVer;
	}
	
	public DefaultTableModel getModelPersonas() 
	{
		return modelPersonas;
	}
	
	public JTable getTablaPersonas()
	{
		return tablaPersonas;
	}

	public String[] getNombreColumnas() 
	{
		return nombreColumnas;
	}
	
	public JButton getBtnConfiguracion() {
		return this.btnConfiguracion;
	}

	
	public JButton getBtnReporteLocal() {
		return btnReporteLocal;
	}


	public void setBtnReporteLocal(JButton btnReporteLocal) {
		this.btnReporteLocal = btnReporteLocal;
	}

	public void llenarTabla(List<PersonaDTO> personasEnTabla) {
		this.getModelPersonas().setRowCount(0); //Para vaciar la tabla
		this.getModelPersonas().setColumnCount(0);
		this.getModelPersonas().setColumnIdentifiers(this.getNombreColumnas());
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");  
		for (PersonaDTO p : personasEnTabla)
		{
			String nombre = p.getNombre();
			String tel = p.getTelefono();
			String email = p.getEmail();
			String fechaCumpleanios = dateFormat.format(p.getFechaCumpleanios());
			String etiqueta = p.getEtiqueta().toString();
			Object[] fila = {nombre, tel,email, fechaCumpleanios, etiqueta};
			this.getModelPersonas().addRow(fila);
		}
	}
}
