package presentacion.vista;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import modelo.TipoDeContacto;

import javax.swing.JComboBox;
import javax.swing.JToolBar;
import javax.swing.JTabbedPane;
import com.jgoodies.forms.factories.DefaultComponentFactory;

import dto.LocalidadDTO;
import dto.ProvinciaDTO;
import dto.TipoDeContactoDTO;
import dto.GeneroMusicalDTO;

import java.awt.Component;
import javax.swing.Box;
import java.awt.Dimension;
import java.awt.Panel;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JSpinner;

public class VentanaPersona extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtNombre;
	private JTextField txtTelefono;
	private JTextField txtEmail;
	private JTextField txtId;
	private JSpinner spinnerPiso;
	JSpinner spinnerAltura;
	
	private JFormattedTextField fechaCumpleanios;
	
	private JButton btnAgregarPersona;
	private JComboBox<String> comboBoxTipoDeContacto;
	private JComboBox<String> comboBoxLocalidad = new JComboBox<String>();
	private JComboBox<String> comboBoxProvincia = new JComboBox<String>();
	private JComboBox<String> comboBoxGeneroMusical = new JComboBox<String>();
	
	private HashMap<String, TipoDeContactoDTO> etiquetas;
	private ArrayList<ProvinciaDTO> provincias;
	private HashMap<String, ArrayList<LocalidadDTO>> diccionarioProvincias = new HashMap<String, ArrayList<LocalidadDTO>>();
	private ArrayList<GeneroMusicalDTO> generosMusicales;
	
	private static VentanaPersona INSTANCE;
	private JTextField textCalle;
	private JTextField textDepartamento;
	
	public static VentanaPersona getInstance(ArrayList<ProvinciaDTO> seleccionProvincias, HashMap<String, TipoDeContactoDTO> seleccionTipoDeContacto, ArrayList<GeneroMusicalDTO> generosMusicales)
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaPersona(seleccionProvincias, seleccionTipoDeContacto, generosMusicales); 	
			return new VentanaPersona(seleccionProvincias, seleccionTipoDeContacto, generosMusicales);
		}
		else
			return INSTANCE;
	}

	private VentanaPersona(ArrayList<ProvinciaDTO> seleccionProvincias, HashMap<String, TipoDeContactoDTO> seleccionTipoDeContacto, ArrayList<GeneroMusicalDTO> generosMusicales) 
	{
		super();
		this.provincias = seleccionProvincias;
		this.etiquetas = seleccionTipoDeContacto;
		this.generosMusicales = generosMusicales;
		this.inicializarDiccionarioDeProvincias();
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 492, 554);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 468, 515);
		contentPane.add(panel);
		panel.setLayout(null);
		// LABELS
		JLabel lblNombreYApellido = new JLabel("Nombre y apellido");
		lblNombreYApellido.setBounds(10, 33, 113, 14);
		panel.add(lblNombreYApellido);
		
		JLabel lblTelfono = new JLabel("Telefono");
		lblTelfono.setBounds(10, 69, 113, 14);
		panel.add(lblTelfono);

		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setBounds(10, 106, 113, 14);
		panel.add(lblEmail);
		
		JLabel lblCumpleanios = new JLabel("Fecha de Nacimiento (yyyy-mm-dd):");
		lblCumpleanios.setBounds(10, 141, 189, 14);
		panel.add(lblCumpleanios);
		
		
		
		// INPUTS
		txtId = new JTextField();
		
		txtNombre = new JTextField();
		txtNombre.setBounds(294, 30, 164, 20);
		panel.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtTelefono = new JTextField();
		txtTelefono.setBounds(294, 66, 164, 20);
		panel.add(txtTelefono);
		txtTelefono.setColumns(10);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(294, 103, 164, 20);
		panel.add(txtEmail);
		txtEmail.setColumns(10);
		
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		fechaCumpleanios = new JFormattedTextField(format);
		fechaCumpleanios.setBounds(294, 138, 164, 20);
		panel.add(fechaCumpleanios);
		fechaCumpleanios.setColumns(10);
		
		btnAgregarPersona = new JButton("Guardar");
		btnAgregarPersona.setBounds(331, 479, 89, 30);
		panel.add(btnAgregarPersona);
		
		JLabel lblTipoDeContacto = new JLabel("Tipo de contacto:");
		lblTipoDeContacto.setBounds(10, 173, 113, 14);
		panel.add(lblTipoDeContacto);
		
		comboBoxTipoDeContacto = new JComboBox<String>();
		comboBoxTipoDeContacto.setBounds(294, 169, 164, 22);
		for(TipoDeContactoDTO t: this.etiquetas.values()) {
			comboBoxTipoDeContacto.addItem(t.getNombre());
		}
		panel.add(comboBoxTipoDeContacto);
		
		JLabel lblPersonalInfo = DefaultComponentFactory.getInstance().createTitle("Información Personal");
		lblPersonalInfo.setBounds(10, 7, 143, 14);
		panel.add(lblPersonalInfo);
		
		JLabel lblDireccion = DefaultComponentFactory.getInstance().createTitle("Información del domicilio");
		lblDireccion.setBounds(6, 228, 171, 14);
		panel.add(lblDireccion);
		
		JLabel lblCalle = new JLabel("Calle");
		lblCalle.setBounds(10, 254, 113, 14);
		panel.add(lblCalle);
		
		textCalle = new JTextField();
		textCalle.setBounds(294, 248, 164, 20);
		panel.add(textCalle);
		textCalle.setColumns(10);
		
		comboBoxProvincia.setBounds(294, 279, 164, 22);
		for(ProvinciaDTO provincia :this.provincias) {
			comboBoxProvincia.addItem(provincia.getNombre());
			comboBoxProvincia.addItemListener(new ItemListener() {

				@Override
				public void itemStateChanged(ItemEvent event) {
					if(event.getStateChange() == ItemEvent.SELECTED) {
						Object item = event.getItem();
						reinicializarOpciones(item.toString());
					}
				}
				
			});
		}
		panel.add(comboBoxProvincia);
		
		JLabel lblProvincia = new JLabel("Provincia");
		lblProvincia.setBounds(10, 283, 189, 14);
		panel.add(lblProvincia);
		
		comboBoxLocalidad.setBounds(294, 312, 164, 22);
		panel.add(comboBoxLocalidad);
		
		JLabel lblLocalidad = new JLabel("Localidad");
		lblLocalidad.setBounds(10, 316, 189, 14);
		panel.add(lblLocalidad);
		
		JLabel lblAltura = new JLabel("Altura");
		lblAltura.setBounds(10, 348, 189, 14);
		panel.add(lblAltura);
		
		JLabel lblPiso = new JLabel("Piso");
		lblPiso.setBounds(10, 379, 189, 14);
		panel.add(lblPiso);
		
		spinnerPiso = new JSpinner();
		spinnerPiso.setBounds(294, 376, 164, 20);
		panel.add(spinnerPiso);
		
		spinnerAltura = new JSpinner();
		spinnerAltura.setBounds(294, 345, 164, 20);
		panel.add(spinnerAltura);
		
		textDepartamento = new JTextField();
		textDepartamento.setColumns(10);
		textDepartamento.setBounds(294, 409, 164, 20);
		panel.add(textDepartamento);
		
		JLabel lblDepartamento = new JLabel("Departamento");
		lblDepartamento.setBounds(10, 412, 189, 14);
		panel.add(lblDepartamento);
		
		comboBoxGeneroMusical.setBounds(294, 442, 164, 22);
		for(GeneroMusicalDTO generoMusical :this.generosMusicales) {
			comboBoxGeneroMusical.addItem(generoMusical.getNombre());
		}
		panel.add(comboBoxGeneroMusical);
		
		JLabel lblGustosPersona = new JLabel("Genero Musical Favorito");
		lblGustosPersona.setBounds(10, 445, 189, 14);
		panel.add(lblGustosPersona);
		
		this.setVisible(false);
	}
	
	public void mostrarVentana()
	{
		this.reiniciarFormulario();
		this.setVisible(true);
	}
	
	public JTextField getTxtNombre() 
	{
		return txtNombre;
	}

	public JTextField getTxtTelefono() 
	{
		return txtTelefono;
	}

	public JButton getBtnAgregarPersona() 
	{
		return btnAgregarPersona;
	}
	
	public void setBtnAgregarPersona(JButton btn) 
	{
		btnAgregarPersona = btn;
	}

	public JTextField getEmail() {
		return txtEmail;
	}

	public void setEmail(String email) {
		this.txtEmail.setText( email);
	}
	
	public void setNombre(String nombre) {
		this.txtNombre.setText( nombre);
	}
	
	public void setTelefono(String tel) {
		this.txtTelefono.setText( tel);
	}
	public void setFechaCumpleanios(Date fecha) {
		this.fechaCumpleanios.setText(fecha.toString());
	}
	
	public JFormattedTextField getFechaCumpleanios() {
		
		return fechaCumpleanios;
	}
	
	public String getEtiqueta() {
		return this.comboBoxTipoDeContacto.getSelectedItem().toString();
	}

	public void setId(Integer id) {
		this.txtId.setText(id.toString());
	}
	
	public String getId() {
		return this.txtId.getText();
	}
	
	public void cerrar()
	{
		this.txtNombre.setText(null);
		this.txtTelefono.setText(null);
		this.dispose();
	}
	
	
	public void reiniciarFormulario() {
		this.txtId.setText("");
		this.txtEmail.setText("");
		this.txtNombre.setText("");
		this.txtTelefono.setText("");
		this.fechaCumpleanios.setText("");
		this.textCalle.setText("");
		this.textDepartamento.setText("");
		this.spinnerAltura.setValue(0);
		this.spinnerPiso.setValue(0);
	}
	
	private void inicializarDiccionarioDeProvincias(){
		for(ProvinciaDTO provincia :this.provincias) {
			this.diccionarioProvincias.put(provincia.getNombre(), provincia.getLocalidades());
		}
	}
	
	private void reinicializarOpciones(String provincia) {
		ArrayList<LocalidadDTO> localidades = this.diccionarioProvincias.get(provincia);
		this.comboBoxLocalidad.removeAllItems();
		for(LocalidadDTO localidad: localidades) {
			this.comboBoxLocalidad.addItem(localidad.getNombre());
		}
	}
	
	public String getCalle() {
		return this.textCalle.getText();
	}

	public void setCalle(String calle) {
		this.textCalle.setText(calle);
	}
	
	public void setLocalidad(String localidad) {
		this.comboBoxLocalidad.setSelectedItem(localidad);
	}
	
	public void setProvincia(String provincia) {
		this.comboBoxProvincia.setSelectedItem(provincia);
	}
	
	public void setGeneroMusical(String generoMusical) {
		this.comboBoxGeneroMusical.setSelectedItem(generoMusical);
	}
	
	public void setDepartamento(String departamento) {
		this.textDepartamento.setText(departamento);
	}
	
	public void setAltura(Integer altura) {
		this.spinnerAltura.setValue(altura);
	}
	
	public void setPiso(Integer piso) {
		this.spinnerPiso.setValue(piso);
	}
	
	public int getAltura() {
		return Integer.parseInt((this.spinnerAltura.getValue().toString()));
	}
	
	public int getPiso() {
		return Integer.parseInt(this.spinnerPiso.getValue().toString());
	}
	
	public String getProvince() {
		return this.comboBoxProvincia.getSelectedItem().toString();
	}
	
	public String getGeneroMusical() {
		return this.comboBoxGeneroMusical.getSelectedItem().toString();
	}
	
	public String getLocalidad() {
		return this.comboBoxLocalidad.getSelectedItem().toString();
	}
	
	public String getDepartamento() {
		return this.textDepartamento.getText();
	}

	public HashMap<String, TipoDeContactoDTO> getEtiquetas() {
		return etiquetas;
	}

	public void setEtiquetas(HashMap<String, TipoDeContactoDTO> etiquetas) {
		this.etiquetas = etiquetas;
	}

	public ArrayList<ProvinciaDTO> getProvincias() {
		return provincias;
	}

	public void setProvincias(ArrayList<ProvinciaDTO> provincias) {
		this.provincias = provincias;
	}

	public HashMap<String, ArrayList<LocalidadDTO>> getDiccionarioProvincias() {
		return diccionarioProvincias;
	}

	public void setDiccionarioProvincias(HashMap<String, ArrayList<LocalidadDTO>> diccionarioProvincias) {
		this.diccionarioProvincias = diccionarioProvincias;
	}
	
	
}

