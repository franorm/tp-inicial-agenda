package presentacion.vista;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import com.jgoodies.forms.factories.DefaultComponentFactory;

import dto.DireccionDTO;
import dto.PersonaDTO;
import dto.ProvinciaDTO;

import java.awt.Font;
import java.util.ArrayList;

public class VentanaPresentacion {

	private JFrame frame;
	private PersonaDTO persona;
	private DireccionDTO direccion;
	private static VentanaPresentacion INSTANCE_PRESENTACION;

	/**
	 * Launch the application.
	 */
	public static VentanaPresentacion getInstance(PersonaDTO persona, DireccionDTO direccion)
	{
		if(INSTANCE_PRESENTACION == null)
		{
			INSTANCE_PRESENTACION = new VentanaPresentacion(persona, direccion); 	
			return new VentanaPresentacion(persona,direccion);
		}
		else
			return INSTANCE_PRESENTACION;
	}

	/**
	 * Create the application.
	 */
	public VentanaPresentacion(PersonaDTO persona, DireccionDTO direccion) {
		this.persona = persona;
		this.direccion = direccion;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 543, 300);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblContacto = DefaultComponentFactory.getInstance().createTitle("Contacto");
		lblContacto.setBounds(10, 11, 91, 14);
		frame.getContentPane().add(lblContacto);
		
		JLabel lblNombre = new JLabel(this.persona.getNombre());
		lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblNombre.setBounds(20, 36, 173, 24);
		frame.getContentPane().add(lblNombre);
		
		JLabel lblEtiqueta = new JLabel(this.persona.getTipoDeContacto());
		lblEtiqueta.setBounds(30, 71, 122, 14);
		frame.getContentPane().add(lblEtiqueta);
		
		JLabel lblCumpleaos = new JLabel("Cumpleaños:");
		lblCumpleaos.setBounds(203, 44, 91, 14);
		frame.getContentPane().add(lblCumpleaos);
		
		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setBounds(203, 71, 48, 14);
		frame.getContentPane().add(lblEmail);
		
		JLabel lblTelfono = new JLabel("Teléfono:");
		lblTelfono.setBounds(203, 96, 91, 14);
		frame.getContentPane().add(lblTelfono);
		
		JLabel lbldataCumpleanos = new JLabel(this.persona.getFechaCumpleanios().toString());
		lbldataCumpleanos.setBounds(304, 44, 216, 14);
		frame.getContentPane().add(lbldataCumpleanos);
		
		JLabel lbldataEmail = new JLabel(this.persona.getEmail());
		lbldataEmail.setBounds(304, 71, 216, 14);
		frame.getContentPane().add(lbldataEmail);
		
		JLabel lblDatotel = new JLabel(this.persona.getTelefono());
		lblDatotel.setBounds(304, 96, 216, 14);
		frame.getContentPane().add(lblDatotel);
		
		JLabel lblProvincia = new JLabel("Provincia:");
		lblProvincia.setBounds(10, 166, 91, 14);
		frame.getContentPane().add(lblProvincia);
		
		JLabel lblLocalidad = new JLabel("Localidad:");
		lblLocalidad.setBounds(10, 191, 91, 14);
		frame.getContentPane().add(lblLocalidad);
		
		JLabel lblDireccion = new JLabel("Direccion:");
		lblDireccion.setBounds(10, 216, 91, 14);
		frame.getContentPane().add(lblDireccion);
		
		JLabel lblPiso = new JLabel("Piso:");
		lblPiso.setBounds(250, 166, 48, 14);
		frame.getContentPane().add(lblPiso);
		
		JLabel lblDepartamento = new JLabel("Departamento:");
		lblDepartamento.setBounds(250, 216, 91, 14);
		frame.getContentPane().add(lblDepartamento);
		
		JLabel lblDatoProvincia = new JLabel(this.direccion.getProvincia());
		lblDatoProvincia.setBounds(111, 166, 122, 14);
		frame.getContentPane().add(lblDatoProvincia);
		
		JLabel lblDatolocalidad = new JLabel(this.direccion.getLocalidad());
		lblDatolocalidad.setBounds(111, 191, 122, 14);
		frame.getContentPane().add(lblDatolocalidad);
		
		JLabel lblDatodireccion = new JLabel(this.direccion.getCalle()+" "+this.direccion.getAltura());
		lblDatodireccion.setBounds(111, 216, 122, 14);
		frame.getContentPane().add(lblDatodireccion);
		
		JLabel lblDatopiso = new JLabel(this.direccion.getPiso()+"");
		lblDatopiso.setBounds(368, 166, 152, 14);
		frame.getContentPane().add(lblDatopiso);
		
		JLabel lblDatodepto = new JLabel(this.direccion.getDepartamento());
		lblDatodepto.setBounds(368, 216, 152, 14);
		frame.getContentPane().add(lblDatodepto);
		
		this.mostrarVentana();
	}
	
	public void mostrarVentana()
	{
		this.frame.setVisible(true);
	}
}
