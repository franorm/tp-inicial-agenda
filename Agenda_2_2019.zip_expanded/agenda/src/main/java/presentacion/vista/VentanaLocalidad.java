package presentacion.vista;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import dto.LocalidadDTO;
import dto.PersonaDTO;
import dto.ProvinciaDTO;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;

public class VentanaLocalidad extends JFrame{
	private static final long serialVersionUID = 1L;
	private JButton btnGuardar;
	private JButton btnAgregar;
	private JButton btnEliminar;
	private JButton btnEditar;
	private JPanel contentPane;
	private JTextField txtLocal;
	private JComboBox comboBoxProvincia;
	private String[] columnas = new String[] {"Localidad"};
	private DefaultTableModel modelo = new DefaultTableModel(null, columnas);
	private HashMap<String, ArrayList<LocalidadDTO>> diccionarioProvincias = new HashMap<String, ArrayList<LocalidadDTO>>();
	
	private static VentanaLocalidad INSTANCE;
	private JTable table;


	public static VentanaLocalidad getInstance(HashMap<String, ArrayList<LocalidadDTO>> diccionarioProvincia)
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaLocalidad(diccionarioProvincia); 	
		}
		return INSTANCE;
	}

	private VentanaLocalidad(HashMap<String, ArrayList<LocalidadDTO>> diccionarioProvincias) 
	{
		super();
		this.diccionarioProvincias = diccionarioProvincias;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 596, 528);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 571, 481);
		contentPane.add(panel);
		panel.setLayout(null);
		// LABELS
		JLabel lblNombreYApellido = new JLabel("Provincia");
		lblNombreYApellido.setBounds(10, 33, 113, 14);
		panel.add(lblNombreYApellido);
		
		JLabel lblTelfono = new JLabel("Localidad");
		lblTelfono.setBounds(10, 69, 113, 14);
		panel.add(lblTelfono);
		
		this.txtLocal = new JTextField();
		txtLocal.setBounds(294, 66, 164, 20);
		panel.add(txtLocal);
		txtLocal.setColumns(10);
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(468, 440, 89, 30);
		panel.add(btnGuardar);
		
		comboBoxProvincia = new JComboBox();
		comboBoxProvincia.setBounds(294, 29, 164, 22);
		comboBoxProvincia.addItem("");
		Set<String> provincias = this.diccionarioProvincias.keySet();
		for(String provincia: provincias) {
			comboBoxProvincia.addItem(provincia);
			comboBoxProvincia.addItemListener(new ItemListener() {

				@Override
				public void itemStateChanged(ItemEvent event) {
					if(event.getStateChange() == ItemEvent.SELECTED) {
						Object item = event.getItem();
						llenarTabla(item.toString());
						}
					}
				}
			);
			
		}
		panel.add(comboBoxProvincia);
		
		btnEliminar = new JButton("Eliminar");
		btnEliminar.setBounds(468, 406, 89, 23);
		panel.add(btnEliminar);
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(472, 65, 89, 23);
		panel.add(btnAgregar);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 119, 448, 351);
		panel.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		table.setModel(this.modelo);
		
		btnEditar = new JButton("Editar");
		btnEditar.setBounds(468, 372, 89, 23);
		panel.add(btnEditar);
		
		}
	
	public void mostrarVentana()
	{
		//this.reiniciarFormulario();
		this.setVisible(true);
	}
	
	
	
	public HashMap<String, ArrayList<LocalidadDTO>> getDiccionarioProvincias() {
		return diccionarioProvincias;
	}

	public void setDiccionarioProvincias(HashMap<String, ArrayList<LocalidadDTO>> diccionarioProvincias) {
		this.diccionarioProvincias = diccionarioProvincias;
	}

	public JButton getBtnAgregar() {
		return this.btnAgregar;
	}
	
	public JButton getBtnEliminar() {
		return this.btnEliminar;
	}
	
	public JButton getBtnEditar() {
		return this.btnEditar;
	}
	
	public JComboBox getProvinciaComboBox() {
		return this.comboBoxProvincia;
	}
	
	public JTextField getLocalidadInput() {
		return this.txtLocal;
	}
	
	public JTable getTableLocalidades() {
		return this.table;
	}
	
	public void llenarTabla(String provincia) {
		ArrayList<LocalidadDTO> localidades = this.diccionarioProvincias.get(provincia);
		this.modelo.setRowCount(0);
		this.modelo.setColumnCount(0);
		this.modelo.setColumnIdentifiers(this.columnas);
		if(!localidades.isEmpty()) {
			for(LocalidadDTO l: localidades) {
				Object[] localidad = {l.getNombre()};
				this.modelo.addRow(localidad);
			}
		}
	}
}
