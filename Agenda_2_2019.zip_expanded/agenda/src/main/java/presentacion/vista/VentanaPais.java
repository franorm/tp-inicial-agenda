package presentacion.vista;

import java.awt.Container;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import dto.LocalidadDTO;
import dto.PaisDTO;
import dto.PersonaDTO;
import dto.ProvinciaDTO;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;

public class VentanaPais extends JFrame{
	private static final long serialVersionUID = 1L;
	private JButton btnGuardar;
	private JButton btnAgregar;
	private JButton btnEliminar;
	private JButton btnEditar;
	private JPanel contentPane;
	private JTextField txtPais;
	private String[] columnas = new String[] {"Pais"};
	private DefaultTableModel modelo = new DefaultTableModel(null, columnas);
	private ArrayList<PaisDTO> paises = new ArrayList<PaisDTO>();
	
	private static VentanaPais INSTANCE;
	private JTable table;


	public static VentanaPais getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaPais(); 	
		}
		return INSTANCE;
	}

	private VentanaPais() 
	{
		super();
		setTitle("Configuración de paises");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 596, 528);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 571, 481);
		contentPane.add(panel);
		panel.setLayout(null);
		// LABELS
		JLabel lblPais = new JLabel("Pais");
		lblPais.setBounds(10, 69, 113, 14);
		panel.add(lblPais);
		
		this.txtPais = new JTextField();
		txtPais.setBounds(294, 66, 164, 20);
		panel.add(txtPais);
		txtPais.setColumns(10);
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(468, 440, 89, 30);
		panel.add(btnGuardar);
		
		btnEliminar = new JButton("Eliminar");
		btnEliminar.setBounds(468, 406, 89, 23);
		panel.add(btnEliminar);
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(472, 65, 89, 23);
		panel.add(btnAgregar);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 119, 448, 351);
		panel.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		table.setModel(this.modelo);
		
		btnEditar = new JButton("Editar");
		btnEditar.setBounds(468, 372, 89, 23);
		panel.add(btnEditar);
		
		}
	
	public void mostrarVentana()
	{
		this.llenarTabla();
		this.setVisible(true);
	}
	
	public JButton getBtnAgregar() {
		return this.btnAgregar;
	}
	
	public JButton getBtnEliminar() {
		return this.btnEliminar;
	}
	
	public JButton getBtnEditar() {
		return this.btnEditar;
	}
	
	public JTextField getPaisInput() {
		return this.txtPais;
	}
	
	public JTable getTablePais() {
		return this.table;
	}
	
	public void llenarTabla() {
		this.modelo.setRowCount(0);
		this.modelo.setColumnCount(0);
		this.modelo.setColumnIdentifiers(this.columnas);
		if(!paises.isEmpty()) {
			for(PaisDTO p: paises) {
				Object[] filaPais = {p.getNombre()};
				this.modelo.addRow(filaPais);
			}
		}
	}
	
	public void setPaises(ArrayList<PaisDTO> paises) {
		this.paises = paises;
	}

	public ArrayList<PaisDTO> getPaises() {
		// TODO Auto-generated method stub
		return this.paises;
	}
}
