package presentacion.vista;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import com.jgoodies.forms.factories.DefaultComponentFactory;

import dto.DireccionDTO;
import dto.PersonaDTO;
import dto.ProvinciaDTO;

import java.awt.Font;
import java.util.ArrayList;
import javax.swing.JTabbedPane;
import javax.swing.text.IconView;

import java.awt.GridLayout;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class VentanaConfiguracion {

	private JFrame frmConfiguracin;
	private PersonaDTO persona;
	private DireccionDTO direccion;
	private static VentanaConfiguracion INSTANCE_PRESENTACION;
	private JButton btnPaises;
	private JButton btnProvincias;
	private JButton btnLocalidades;

	/**
	 * Launch the application.
	 */
	public static VentanaConfiguracion getInstance()
	{
		if(INSTANCE_PRESENTACION == null)
		{
			INSTANCE_PRESENTACION = new VentanaConfiguracion(); 	
			return new VentanaConfiguracion();
		}
		else
			return INSTANCE_PRESENTACION;
	}

	/**
	 * Create the application.
	 */
	public VentanaConfiguracion() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmConfiguracin = new JFrame();
		frmConfiguracin.setTitle("Configuración");
		frmConfiguracin.setBounds(100, 100, 292, 154);
		frmConfiguracin.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmConfiguracin.getContentPane().setLayout(null);
		
		btnPaises = new JButton("Paises");
		btnPaises.setBounds(49, 11, 180, 23);
		frmConfiguracin.getContentPane().add(btnPaises);
		
		btnProvincias = new JButton("Provincias");
		btnProvincias.setBounds(49, 45, 180, 23);
		frmConfiguracin.getContentPane().add(btnProvincias);
		
		btnLocalidades = new JButton("Localidades");
		btnLocalidades.setBounds(49, 79, 180, 23);
		frmConfiguracin.getContentPane().add(btnLocalidades);
		
		Icon iconProvincia = new ImageIcon("../agenda/media/icon1.png");
	}
	
	public void mostrarVentana()
	{
		this.frmConfiguracin.setVisible(true);
	}

	public JButton getBtnPaises() {
		return btnPaises;
	}

	public JButton getBtnProvincias() {
		return btnProvincias;
	}

	public JButton getBtnLocalidades() {
		return btnLocalidades;
	}
}
