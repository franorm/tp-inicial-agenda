package presentacion.vista;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import dto.PaisDTO;
import dto.ProvinciaDTO;

public class SeleccionPais extends JFrame{
	



	private static final long serialVersionUID = 1L;
	private JButton btnElegirPais;
	private JPanel contentPane;
	
	private JComboBox<String> comboBoxPais = new JComboBox<String>();
	
	
	private ArrayList<PaisDTO> paises = new ArrayList<PaisDTO>();
	
	private static SeleccionPais INSTANCE;

	public static SeleccionPais getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new SeleccionPais(); 	
		}
		return INSTANCE;
	}
	
	
	private SeleccionPais() 
		{
			super();
			setTitle("Seleccionar Pais");
			setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			setBounds(100, 100, 270, 200);
			contentPane = new JPanel();
			contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			setContentPane(contentPane);
			contentPane.setLayout(null);
			
			JPanel panel = new JPanel();
			panel.setBounds(10, 11, 270, 200);
			contentPane.add(panel);
			panel.setLayout(null);
			// LABELS
			JLabel lblPais = new JLabel("Pais");
			lblPais.setBounds(10, 69, 113, 14);
			panel.add(lblPais);
			
			comboBoxPais.setBounds(50, 69, 164, 22);
			panel.add(comboBoxPais);
			
			
			btnElegirPais = new JButton("Elegir");
			btnElegirPais.setBounds(10, 100, 89, 30);
			panel.add(btnElegirPais);
			
		
		}
	
	public void setPaises(ArrayList<PaisDTO> paises) {
		this.paises = paises;
		for(PaisDTO pais :this.paises) {
			comboBoxPais.addItem(pais.getNombre());
		}
	}
	
	public void ocultarVentana()
	{
		this.setVisible(false);
	}
	
	public void mostrarVentana()
	{
		this.setVisible(true);
	}

	
	
	public JButton getBtnElegirPais() {
		return btnElegirPais;
	}


	public void setBtnElegirPais(JButton btnElegirPais) {
		this.btnElegirPais = btnElegirPais;
	}
	
	
	public String getPais() {
		return this.comboBoxPais.getSelectedItem().toString();
	}
}
