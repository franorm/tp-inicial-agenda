package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import modelo.Agenda;
import persistencia.conexion.Conexion;
import presentacion.reportes.ReporteAgenda;
import presentacion.reportes.ReporteLocalidades;
import presentacion.vista.VentanaPersona;
import presentacion.vista.VentanaPresentacion;
import presentacion.vista.VentanaProvincia;
import presentacion.vista.SeleccionPais;
import presentacion.vista.VentanaConfiguracion;
import presentacion.vista.VentanaLocalidad;
import presentacion.vista.VentanaPais;
import presentacion.vista.Vista;
import dto.DireccionDTO;
import dto.LocalidadDTO;
import dto.PaisDTO;
import dto.PersonaDTO;
import dto.PersonasPorLocalidadesDTO;
import dto.ProvinciaDTO;
import dto.TipoDeContactoDTO;
import dto.GeneroMusicalDTO;

public class Controlador implements ActionListener
{
		private Vista vista;
		private SeleccionPais ventanaSelecccionPais;
		private List<PersonaDTO> personasEnTabla;
		
		private HashMap<Integer, DireccionDTO> direccionesEnTabla;
		private ArrayList<ProvinciaDTO> seleccionProvincias;
		private HashMap<String, TipoDeContactoDTO> seleccionTipoDeContacto;
		private HashMap<String, ArrayList<LocalidadDTO>> diccionarioProvincias = new HashMap<String, ArrayList<LocalidadDTO>>();
		private HashMap<String, ArrayList<ProvinciaDTO>> diccionarioPaises;
		private ArrayList<GeneroMusicalDTO> generosMusicalesEnTabla;
		
		private VentanaPersona ventanaPersona; 
		private VentanaPresentacion presentacion;
		private VentanaLocalidad ventanaLocalidad;
		private VentanaProvincia ventanaProvincia;
		private VentanaPais ventanaPais;
		private VentanaConfiguracion ventanaConfiguracion;
		
		private Agenda agenda;
		private int originalIdPersona = 0, originalIdDireccion = 0;
		
		
		private String PaisSeleccionado;
		
		public Controlador(Vista vista, Agenda agenda)
		{
			this.vista = vista;
			this.seleccionProvincias = agenda.obtenerProvincias();
			this.seleccionTipoDeContacto = agenda.obtenerMapaEtiquetas();
			this.diccionarioPaises = agenda.getMapPaises();
			this.inicializarDiccionarioDeProvincias();
			this.generosMusicalesEnTabla = agenda.obtenerGenerosMusicales();
			
			this.ventanaSelecccionPais = ventanaSelecccionPais.getInstance();
			this.ventanaSelecccionPais.getBtnElegirPais().addActionListener(e -> this.mostrarReporteLocalidades(e));
			
			this.ventanaConfiguracion = VentanaConfiguracion.getInstance();
			this.ventanaConfiguracion.getBtnLocalidades().addActionListener(e -> this.altaLocalidad(e));
			this.ventanaConfiguracion.getBtnProvincias().addActionListener(e -> this.altaProvincia(e));
			this.ventanaConfiguracion.getBtnPaises().addActionListener(e -> this.altaPais(e));
			
			this.ventanaLocalidad = VentanaLocalidad.getInstance(this.diccionarioProvincias);
			this.ventanaLocalidad.getBtnAgregar().addActionListener(a -> this.agregarLocalidad(a));
			this.ventanaLocalidad.getBtnEliminar().addActionListener(a -> this.eliminarLocalidad(a));
			this.ventanaLocalidad.getBtnEditar().addActionListener(a -> this.editarLocalidad(a));
			
			this.ventanaProvincia = VentanaProvincia.getInstance(this.diccionarioPaises);
			this.ventanaProvincia.getBtnAgregar().addActionListener(a -> this.agregarProvincia(a));
			this.ventanaProvincia.getBtnEliminar().addActionListener(a -> this.eliminarProvincia(a));
			this.ventanaProvincia.getBtnEditar().addActionListener(a -> this.editarProvincia(a));
			
			this.ventanaPais = VentanaPais.getInstance();
			this.ventanaPais.getBtnAgregar().addActionListener(a -> this.agregarPais(a));
			this.ventanaPais.getBtnEliminar().addActionListener(a -> this.eliminarPais(a));
			this.ventanaPais.getBtnEditar().addActionListener(a -> this.editarPais(a));
			
			this.vista.getBtnAgregar().addActionListener(a->ventanaAgregarPersona(a));
			this.vista.getBtnBorrar().addActionListener(s->borrarPersona(s));
			this.vista.getBtnReporte().addActionListener(r->mostrarReporteContactos(r));
			this.vista.getBtnReporteLocal().addActionListener(r->SeleccionarPaisReporte(r));
			this.vista.getBtnEditar().addActionListener(a->editarPersona(a));
			this.vista.getBtnVer().addActionListener(a -> verPersona(a));
			this.vista.getBtnConfiguracion().addActionListener(p -> mostrarConfiguracion(p));
			
			this.ventanaPersona = VentanaPersona.getInstance(this.seleccionProvincias, this.seleccionTipoDeContacto, this.generosMusicalesEnTabla);
			this.ventanaPersona.getBtnAgregarPersona().addActionListener(p->guardarPersona(p));

			this.agenda = agenda;
			this.ventanaPais.setPaises(this.agenda.getPaises());
			this.ventanaSelecccionPais.setPaises(this.agenda.getPaises());
			
			
		}
		
		
		
		private void SeleccionarPaisReporte(ActionEvent r) {
			// TODO Auto-generated method stub
			this.ventanaSelecccionPais.mostrarVentana();
			

		}
		
		private void mostrarReporteLocalidades(ActionEvent r) {
			// TODO Auto-generated method stub
			this.PaisSeleccionado = ventanaSelecccionPais.getPais();
			ventanaSelecccionPais.ocultarVentana();
			int pais = this.agenda.getPaisByName(this.PaisSeleccionado).getIdPais();
			List<PersonasPorLocalidadesDTO> dataset = agenda.getDataSetReporteLocalidades(pais);
			ReporteLocalidades reporte = new ReporteLocalidades(this.agenda.getConexion());
			reporte.mostrar();	
		}

		private void altaPais(ActionEvent e) {
			// TODO Auto-generated method stub
			this.ventanaPais.mostrarVentana();
		}

		private void ventanaAgregarPersona(ActionEvent a) {
			this.ventanaPersona.mostrarVentana();
		}
		
		private void mostrarConfiguracion(ActionEvent e) {
			this.ventanaConfiguracion.mostrarVentana();
		}
		
		private void altaLocalidad(ActionEvent r)  {
			this.ventanaLocalidad.mostrarVentana();
		}

		private void altaProvincia(ActionEvent e) {
			this.ventanaProvincia.mostrarVentana();
		}
		
		private boolean existeContacto(String nombre) {
			// es para validar si el nombre es valido 
			for( PersonaDTO fila : this.personasEnTabla) {
				
				if(fila.getNombre().equals(nombre)) {
					return true;
				}
			}
			return false;
		}
		
		private void guardarPersona(ActionEvent p)  {
			String nombre = this.ventanaPersona.getTxtNombre().getText();
			String tel = ventanaPersona.getTxtTelefono().getText();
			String email = ventanaPersona.getEmail().getText();
			String fechaCumpleanios = ventanaPersona.getFechaCumpleanios().getText();	
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date fechaCumpleaniosDate = null;
			String etiqueta = ventanaPersona.getEtiqueta();
			TipoDeContactoDTO tipoDeContacto = this.seleccionTipoDeContacto.get(etiqueta);
			GeneroMusicalDTO favorito = this.agenda.obtenerGeneroPorNombre(this.ventanaPersona.getGeneroMusical());
			
			String calle = this.ventanaPersona.getCalle(), localidad = this.ventanaPersona.getLocalidad(), nombreProvincia = this.ventanaPersona.getProvince(), departamento = this.ventanaPersona.getDepartamento();
			int altura = this.ventanaPersona.getAltura(), piso = this.ventanaPersona.getPiso();
			
			if(!fechaCumpleanios.equals("")) {
				try {
					fechaCumpleaniosDate = (Date) formatter.parse(fechaCumpleanios);
				}
				catch(ParseException e) {
					e.printStackTrace();
				}
			}else {
				try {
					fechaCumpleaniosDate = (Date) formatter.parse("1999-01-01");
				}
				catch(ParseException e) {
					e.printStackTrace();
				}
			}

			if(this.originalIdDireccion+this.originalIdPersona != 0) {
				@SuppressWarnings("deprecation")
				int nuevoId = PersonaDTO.calculateIdPersona(nombre, tel);
				PersonaDTO persona = new PersonaDTO(nuevoId,nombre, tel,email,fechaCumpleaniosDate, tipoDeContacto, favorito);
				this.agenda.editarPersona(persona, this.originalIdPersona);
				ProvinciaDTO provincia = this.agenda.getProvinciaByName(nombreProvincia);
				
				DireccionDTO direccionAActualizar = new DireccionDTO(calle,localidad,provincia.getNombre(),departamento,altura, piso,nuevoId, provincia.getIdProvincia());
				direccionAActualizar.setIdDireccion(direccionAActualizar.hashCode());
				
				this.agenda.editarDireccion(direccionAActualizar, this.originalIdDireccion);
			}else {
				PersonaDTO nuevaPersona = new PersonaDTO(0, nombre, tel,email,fechaCumpleaniosDate, tipoDeContacto, favorito);
				int idPersona = nuevaPersona.hashCode();
				nuevaPersona.setIdPersona(idPersona);
				this.agenda.agregarPersona(nuevaPersona);
				this.agregarDireccion(idPersona);
				
			}
			
			this.refrescarTabla();
			this.ventanaPersona.reiniciarFormulario();
			this.ventanaPersona.cerrar();
			this.originalIdDireccion = 0;
			this.originalIdPersona = 0;
		}

		private void mostrarReporteContactos(ActionEvent r) {
			ReporteAgenda reporte = new ReporteAgenda(agenda.obtenerPersonas());
			System.out.println(reporte.toString());
			System.out.println(111111);
			reporte.mostrar();	
		}

		public void borrarPersona(ActionEvent s)
		{
			
			int[] filasSeleccionadas = this.vista.getTablaPersonas().getSelectedRows();
			for (int fila : filasSeleccionadas)
			{
				this.agenda.borrarPersona(this.personasEnTabla.get(fila));
			}
			
			this.refrescarTabla();
		}
			
		@SuppressWarnings("deprecation")
		public void verPersona(ActionEvent s)
		{			
			int[] filasSeleccionadas = this.vista.getTablaPersonas().getSelectedRows();
			if(filasSeleccionadas.length > 0) {
				int filaSeleccionada = this.vista.getTablaPersonas().getSelectedRows()[0];
				
				PersonaDTO personaSeleccionada = this.personasEnTabla.get(filaSeleccionada);
				
				this.ventanaPersona.setNombre(personaSeleccionada.getNombre());
				this.ventanaPersona.setTelefono(personaSeleccionada.getTelefono());
				this.ventanaPersona.setFechaCumpleanios(personaSeleccionada.getFechaCumpleanios());
				this.ventanaPersona.setEmail(personaSeleccionada.getEmail());
				
				
				DireccionDTO direccionDeLaPersonaSeleccionada = this.direccionesEnTabla.get(personaSeleccionada.getIdPersona());
				
				this.ventanaPersona.setCalle(direccionDeLaPersonaSeleccionada.getCalle());
				this.ventanaPersona.setLocalidad(direccionDeLaPersonaSeleccionada.getLocalidad());
				this.ventanaPersona.setProvincia(direccionDeLaPersonaSeleccionada.getProvincia());
				this.ventanaPersona.setDepartamento(direccionDeLaPersonaSeleccionada.getDepartamento());
				this.ventanaPersona.setAltura(direccionDeLaPersonaSeleccionada.getAltura());
				this.ventanaPersona.setPiso(direccionDeLaPersonaSeleccionada.getPiso());

				this.presentacion = new VentanaPresentacion(personaSeleccionada, direccionDeLaPersonaSeleccionada);
			}

		}
		
		public void editarPersona(ActionEvent e) {
			int[] filasSeleccionadas = this.vista.getTablaPersonas().getSelectedRows();
				if(filasSeleccionadas.length > 0) {
					this.ventanaPersona.mostrarVentana();
					int filaSeleccionada = this.vista.getTablaPersonas().getSelectedRows()[0];
					
					PersonaDTO personaSeleccionada = this.personasEnTabla.get(filaSeleccionada);
					
					this.ventanaPersona.setNombre(personaSeleccionada.getNombre());
					this.ventanaPersona.setTelefono(personaSeleccionada.getTelefono());
					this.ventanaPersona.setFechaCumpleanios(personaSeleccionada.getFechaCumpleanios());
					this.ventanaPersona.setEmail(personaSeleccionada.getEmail());
					
					DireccionDTO direccionDeLaPersonaSeleccionada = this.direccionesEnTabla.get(personaSeleccionada.getIdPersona());
					
					this.ventanaPersona.setCalle(direccionDeLaPersonaSeleccionada.getCalle());
					this.ventanaPersona.setLocalidad(direccionDeLaPersonaSeleccionada.getLocalidad());
					this.ventanaPersona.setProvincia(direccionDeLaPersonaSeleccionada.getProvincia());
					this.ventanaPersona.setDepartamento(direccionDeLaPersonaSeleccionada.getDepartamento());
					this.ventanaPersona.setAltura(direccionDeLaPersonaSeleccionada.getAltura());
					this.ventanaPersona.setPiso(direccionDeLaPersonaSeleccionada.getPiso());
					
					this.originalIdPersona = personaSeleccionada.getIdPersona();
					this.originalIdDireccion = direccionDeLaPersonaSeleccionada.getIdDireccion();
					
				}
		}
		
		public void inicializar()
		{
			this.refrescarTabla();
			this.vista.show();
		}
		
		private void refrescarTabla()
		{
			this.personasEnTabla = agenda.obtenerPersonas();
			this.direccionesEnTabla = (HashMap<Integer, DireccionDTO>) agenda.obtenerMapaDirecciones();
			this.vista.llenarTabla(this.personasEnTabla);
		}

		@Override
		public void actionPerformed(ActionEvent e) { }
		
		
		private void agregarDireccion(int idPersona) {
			int altura = this.ventanaPersona.getAltura(), piso = this.ventanaPersona.getPiso();
			String calle = this.ventanaPersona.getCalle(), localidad = this.ventanaPersona.getLocalidad();
			String departamento = this.ventanaPersona.getDepartamento();
			ProvinciaDTO provincia = this.agenda.getProvinciaByName(this.ventanaPersona.getProvince());
			DireccionDTO direccion = new DireccionDTO(calle, localidad, provincia.getNombre(), departamento, altura, piso, idPersona,provincia.getIdProvincia());
			direccion.setIdDireccion(direccion.hashCode());
			this.agenda.agregarDireccion(direccion);
			
		}
		
		private void agregarLocalidad(ActionEvent e) {
			if(!this.ventanaLocalidad.getLocalidadInput().getText().isEmpty()) {
				String nombre = this.ventanaLocalidad.getProvinciaComboBox().getSelectedItem().toString();
				ProvinciaDTO provincia = this.agenda.getProvinciaByName(nombre);
				LocalidadDTO nuevaLocalidad = new LocalidadDTO(this.ventanaLocalidad.getLocalidadInput().getText(),this.ventanaLocalidad.getLocalidadInput().getText().hashCode(),provincia.getNombre(), provincia.getIdProvincia());
				this.agenda.agregarLocalidad(nuevaLocalidad);
				this.ventanaLocalidad.setDiccionarioProvincias(this.agenda.obtenerLocalidadesPorProvincia());
				this.ventanaPersona.setDiccionarioProvincias(this.agenda.obtenerLocalidadesPorProvincia());
				this.ventanaLocalidad.getLocalidadInput().setText("");
				this.ventanaLocalidad.llenarTabla(nombre);
			}else {
				JOptionPane.showMessageDialog(null, "El nombre de la localidad a agregar no debe estar vacío.");
			}
		}
		
		private void eliminarLocalidad(ActionEvent e) {
			int[] filasSeleccionadas = this.ventanaLocalidad.getTableLocalidades().getSelectedRows();
			if(filasSeleccionadas.length > 0) {
				int filaSeleccionada = this.ventanaLocalidad.getTableLocalidades().getSelectedRows()[0];
				String nombreProvincia = this.ventanaLocalidad.getProvinciaComboBox().getSelectedItem().toString();
				LocalidadDTO localidad = this.diccionarioProvincias.get(nombreProvincia).get(filaSeleccionada);
				this.agenda.eliminarLocalidad(localidad);
				this.ventanaLocalidad.setDiccionarioProvincias(this.agenda.obtenerLocalidadesPorProvincia());
				this.ventanaPersona.setDiccionarioProvincias(this.agenda.obtenerLocalidadesPorProvincia());
				this.ventanaLocalidad.llenarTabla(nombreProvincia);
			}else {
				JOptionPane.showMessageDialog(null, "Debe seleccionar una localidad.");
			}			
		}
		
		private void editarLocalidad(ActionEvent e) {
			int[] filasSeleccionadas = this.ventanaLocalidad.getTableLocalidades().getSelectedRows();
			String nombre = "";
			if(!this.ventanaLocalidad.getLocalidadInput().getText().isEmpty()) {
				nombre = this.ventanaLocalidad.getLocalidadInput().getText();
			}else {
				JOptionPane.showMessageDialog(null, "El nombre de la localidad a agregar no debe estar vacío.");
				return;
			}
			if(filasSeleccionadas.length > 0) {
				int filaSeleccionada = this.ventanaLocalidad.getTableLocalidades().getSelectedRows()[0];
				LocalidadDTO localidad = this.diccionarioProvincias.get(nombre).get(filaSeleccionada);
				localidad.setNombre(nombre);
				this.agenda.actualizarLocalidad(localidad);
				this.ventanaLocalidad.setDiccionarioProvincias(this.agenda.obtenerLocalidadesPorProvincia());
				this.ventanaPersona.setDiccionarioProvincias(this.agenda.obtenerLocalidadesPorProvincia());
				this.diccionarioProvincias.get(nombre).get(filaSeleccionada).setNombre(nombre);
				this.ventanaLocalidad.getLocalidadInput().setText("");
			}else {
				JOptionPane.showMessageDialog(null, "Debe seleccionar una localidad.");
				return;
			}	
		}

		private void agregarProvincia(ActionEvent e) {
			if(!this.ventanaProvincia.getProvinciaInput().getText().isEmpty()) {
				String nombreProvincia = this.ventanaProvincia.getProvinciaInput().getText();
				PaisDTO pais = this.agenda.getPaisByName(this.ventanaProvincia.getProvinciaComboBox().getSelectedItem().toString());
				ProvinciaDTO nuevaProvincia = new ProvinciaDTO(nombreProvincia, new ArrayList<LocalidadDTO>(), nombreProvincia.hashCode(),pais.getIdPais());
				this.agenda.agregarProvincia(nuevaProvincia);
				this.diccionarioPaises.get(pais.getNombre()).add(nuevaProvincia);
				this.ventanaPais.getPaisInput().setText("");
				this.ventanaProvincia.setDiccionarioProvincias(this.agenda.getMapPaises());
				this.ventanaPersona.setProvincias(this.agenda.obtenerProvincias());
				this.ventanaPersona.setDiccionarioProvincias(this.agenda.obtenerLocalidadesPorProvincia());
				this.ventanaPais.llenarTabla();
				this.ventanaPais.setPaises(this.agenda.getPaises());
			}else {
				JOptionPane.showMessageDialog(null, "El nombre de la provincia a agregar no debe estar vacío.");
				return;
			}
		}
		
		private void eliminarProvincia(ActionEvent e) {
			int[] filasSeleccionadas = this.ventanaProvincia.getTableProvincia().getSelectedColumns();
			if(filasSeleccionadas.length > 0) {
				int filaSeleccionada = this.ventanaProvincia.getTableProvincia().getSelectedRows()[0];
				String nombrePais = this.ventanaProvincia.getProvinciaComboBox().getSelectedItem().toString();
				ProvinciaDTO provincia = this.diccionarioPaises.get(nombrePais).get(filaSeleccionada);
				this.agenda.eliminarProvincia(provincia);
				this.diccionarioPaises.get(nombrePais).remove(provincia);
				this.ventanaProvincia.setDiccionarioProvincias(this.agenda.getMapPaises());
				this.ventanaPersona.setProvincias(this.agenda.obtenerProvincias());
				this.ventanaPersona.setDiccionarioProvincias(this.agenda.obtenerLocalidadesPorProvincia());
				this.ventanaProvincia.llenarTabla(nombrePais);
			}else {
				JOptionPane.showMessageDialog(null, "Debe seleccionar una provincia.");
			}	
		}
		
		private void editarProvincia(ActionEvent e) {
			int[] filasSeleccionadas = this.ventanaProvincia.getTableProvincia().getSelectedColumns();
			String nombreProvincia = "", nombrePais = this.ventanaProvincia.getProvinciaComboBox().getSelectedItem().toString();
			if(!this.ventanaProvincia.getProvinciaInput().getText().isEmpty()) {
				nombreProvincia = this.ventanaProvincia.getProvinciaInput().getText();
			}else {
				JOptionPane.showMessageDialog(null, "El nombre de la localidad a agregar no debe estar vacío.");
				return;
			}
			if(filasSeleccionadas.length > 0) {
				int filaSeleccionada = this.ventanaProvincia.getTableProvincia().getSelectedRows()[0];
				ProvinciaDTO provincia = this.diccionarioPaises.get(nombrePais).get(filaSeleccionada);
				provincia.setNombre(nombreProvincia);
				this.agenda.actualizarProvincia(provincia);
				this.diccionarioPaises.get(nombrePais).get(filaSeleccionada).setNombre(nombreProvincia);
				this.ventanaProvincia.getProvinciaInput().setText("");
				this.ventanaProvincia.setDiccionarioProvincias(this.agenda.getMapPaises());
				this.ventanaPersona.setProvincias(this.agenda.obtenerProvincias());
				this.ventanaPersona.setDiccionarioProvincias(this.agenda.obtenerLocalidadesPorProvincia());
				this.ventanaProvincia.llenarTabla(nombrePais);
			}else {
				JOptionPane.showMessageDialog(null, "Debe seleccionar una localidad.");
				return;
			}	
		}
		
		private void agregarPais(ActionEvent e) {
			if(!this.ventanaPais.getPaisInput().getText().isEmpty()) {
				String nombrePais = this.ventanaPais.getPaisInput().getText();
				PaisDTO nuevoPais = new PaisDTO(1,nombrePais, new ArrayList<ProvinciaDTO>());
				nuevoPais.setIdPais(nuevoPais.hashCode());
				this.agenda.agregarPais(nuevoPais);
				this.diccionarioPaises.put(nombrePais, new ArrayList<ProvinciaDTO>());
				this.ventanaPais.getPaisInput().setText("");
				this.ventanaPais.setPaises(this.agenda.getPaises());
				this.ventanaPais.llenarTabla();
			}else {
				JOptionPane.showMessageDialog(null, "El nombre del pais a agregar no debe estar vacío.");
				return;
			}
		}
		
		private void eliminarPais(ActionEvent e) {
			int[] filasSeleccionadas = this.ventanaPais.getTablePais().getSelectedColumns();
			if(filasSeleccionadas.length > 0) {
				int filaSeleccionada = this.ventanaPais.getTablePais().getSelectedRows()[0];
				PaisDTO pais = this.ventanaPais.getPaises().get(filaSeleccionada);
				this.agenda.eliminarPais(pais);
				this.ventanaPais.getPaises().remove(filaSeleccionada);
				this.ventanaPais.setPaises(this.agenda.getPaises());
				this.ventanaPais.llenarTabla();
			}else {
				JOptionPane.showMessageDialog(null, "Debe seleccionar un pais.");
			}	
		}
		
		private void editarPais(ActionEvent e) {
			String nombrePais = "";
			if(!this.ventanaPais.getPaisInput().getText().isEmpty()) {
				nombrePais = this.ventanaPais.getPaisInput().getText();
			}else {
				JOptionPane.showMessageDialog(null, "El nombre del pais a agregar no debe estar vacío.");
				return;
			}
			int[] filasSeleccionadas = this.ventanaPais.getTablePais().getSelectedColumns();
			if(filasSeleccionadas.length > 0) {
				int filaSeleccionada = this.ventanaPais.getTablePais().getSelectedRows()[0];
				PaisDTO pais = this.ventanaPais.getPaises().get(filaSeleccionada);
				pais.setNombre(nombrePais);
				this.agenda.actualizarPais(pais, pais.getIdPais());
				this.ventanaPais.getPaises().get(filaSeleccionada).setNombre(nombrePais);;
				this.ventanaPais.getPaisInput().setText("");
				this.ventanaPais.setPaises(this.agenda.getPaises());
				this.ventanaPais.llenarTabla();
			}else {
				JOptionPane.showMessageDialog(null, "Debe seleccionar una localidad.");
				return;
			}	
		}
		
		private void inicializarDiccionarioDeProvincias(){
			for(ProvinciaDTO provincia :this.seleccionProvincias) {
				this.diccionarioProvincias.put(provincia.getNombre(), provincia.getLocalidades());
			}
		}
		
		private void showConfiguracion(ActionEvent e) {
			this.ventanaConfiguracion.mostrarVentana();
		}
}
