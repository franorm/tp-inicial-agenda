package dto;

import java.util.ArrayList;
import java.util.Arrays;

public class GeneroMusicalDTO {
	
	private int idGeneroMusical;
	private String nombre;
	
	public GeneroMusicalDTO(int idGeneroMusical, String nombre){
		this.idGeneroMusical = idGeneroMusical;
		this.nombre = nombre;
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public int getId() {
		return this.idGeneroMusical;
	}

	@Override
	public String toString() {
		return "GeneroMusicalDTO [nombre=" + nombre;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}
	
	public static int calculateIdGeneroMusical(Object nombre) {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}
}
