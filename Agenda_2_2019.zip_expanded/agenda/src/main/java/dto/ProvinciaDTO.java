package dto;

import java.util.ArrayList;
import java.util.Arrays;

public class ProvinciaDTO {
	private String nombre;
	private ArrayList<LocalidadDTO> localidades;
	private int idProvincia, idPais;
	
	public ProvinciaDTO(String nombre, ArrayList<LocalidadDTO> localidades, int idProvincia, int idPais){
		this.setNombre(nombre);
		this.setLocalidades(localidades);
		this.setIdProvincia(idProvincia);
		this.setIdPais(idPais);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public ArrayList<LocalidadDTO> getLocalidades() {
		return localidades;
	}

	public void setLocalidades(ArrayList<LocalidadDTO> localidades) {
		this.localidades = localidades;
	}
	

	public int getIdPais() {
		return idPais;
	}

	public void setIdPais(int idPais) {
		this.idPais = idPais;
	}

	public void setIdProvincia(int idProvincia) {
		this.idProvincia = idProvincia;
	}

	@Override
	public String toString() {
		return "ProvinciaDTO [nombre=" + nombre + ", localidades=" + this.localidades.toString() + "]";
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idPais;
		result = prime * result + idProvincia;
		result = prime * result + ((localidades == null) ? 0 : localidades.hashCode());
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProvinciaDTO other = (ProvinciaDTO) obj;
		if (idPais != other.idPais)
			return false;
		if (idProvincia != other.idProvincia)
			return false;
		if (localidades == null) {
			if (other.localidades != null)
				return false;
		} else if (!localidades.equals(other.localidades))
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}

	public int getIdProvincia() {
		// TODO Auto-generated method stub
		return this.idProvincia;
	}

}
