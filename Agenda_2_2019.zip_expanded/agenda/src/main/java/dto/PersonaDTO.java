package dto;

import java.util.Date;

import modelo.TipoDeContacto;


public class PersonaDTO 
{

	private int idPersona;
	private String nombre;
	private String telefono;
	private String email;
	private Date fechaCumpleanios;
	private TipoDeContactoDTO etiqueta;
	private GeneroMusicalDTO generoFavorito;

	public PersonaDTO(int idPersona, String nombre, String telefono, String email, Date fechaCumpleanios, TipoDeContactoDTO etiqueta, GeneroMusicalDTO generoFavorito)
	{
		this.idPersona = idPersona;
		this.nombre = nombre;
		this.telefono = telefono;
		this.email = email;
		this.fechaCumpleanios = fechaCumpleanios;
		this.setEtiqueta(etiqueta);
		this.setGeneroFavorito(generoFavorito);
	}
	
	private void setGeneroFavorito(GeneroMusicalDTO generoFavorito) {
		// TODO Auto-generated method stub
		this.generoFavorito = generoFavorito;
	}

	
	public int getIdPersona() 
	{
		return this.idPersona;
	}

	public void setIdPersona(int idPersona) 
	{
		this.idPersona = idPersona;
	}

	public String getNombre() 
	{
		return this.nombre;
	}

	public void setNombre(String nombre) 
	{
		this.nombre = nombre;
	}

	public String getTelefono() 
	{
		return this.telefono;
	}

	public void setTelefono(String telefono) 
	{
		this.telefono = telefono;
	}
	public String getEmail() 
	{
		return this.email;
	}

	public Date getFechaCumpleanios() {
		return fechaCumpleanios;
	}

	public void setFechaCumpleanios(Date fechaCumpleanios) {
		this.fechaCumpleanios = fechaCumpleanios;
	}

	public void setEtiqueta(TipoDeContactoDTO etiqueta) {
		this.etiqueta = etiqueta;
	}
	
	public TipoDeContactoDTO getEtiqueta() {
		// TODO Auto-generated method stub
		return this.etiqueta;
	}

	public String getTipoDeContacto() {
		// TODO Auto-generated method stub
		return this.etiqueta.toString();
	}

	public GeneroMusicalDTO getGeneroFavorito() {
		return generoFavorito;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		result = prime * result + ((telefono == null) ? 0 : telefono.hashCode());
		return result;
	}
	
	public static int calculateIdPersona(Object nombre, Object telefono) {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		result = prime * result + ((telefono == null) ? 0 : telefono.hashCode());
		return result;
	}
	
}
