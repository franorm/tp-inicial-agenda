package dto;

import java.util.ArrayList;

public class PaisDTO {
	private int idPais;
	private String nombre;
	private ArrayList<ProvinciaDTO> provincias;
	
	public PaisDTO(int idPais, String nombre, ArrayList<ProvinciaDTO> provincias) {
		this.setIdPais(idPais);
		this.setNombre(nombre);
		this.setProvincias(provincias);
	}

	public int getIdPais() {
		return idPais;
	}
	
	public void setIdPais(int idPais) {
		this.idPais = idPais;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		
		this.nombre = nombre;
	}
	public ArrayList<ProvinciaDTO> getProvincias() {
		return provincias;
	}
	public void setProvincias(ArrayList<ProvinciaDTO> provincias) {
		this.provincias = provincias;
	}
	
	public void agregarProvincia(ProvinciaDTO provinciaAAgregar) {
		for(ProvinciaDTO provincia :this.provincias) {
			if(provincia.getNombre().equals(provinciaAAgregar.getNombre()))
				throw new IllegalArgumentException("La provincia ya pertenece al País");
		}this.provincias.add(provinciaAAgregar);
	}
	
	public boolean eliminarProvincia(ProvinciaDTO provinciaAEliminar) {
		for(int i = 0; i < this.provincias.size(); i++) {
			ProvinciaDTO provincia = this.provincias.get(i);
			if(provincia.getNombre().equals(provinciaAEliminar.getNombre())) {
				this.provincias.remove(i);
				return true;
			}
		}return false;
	}
	
	public boolean existeProvincia(ProvinciaDTO provinciaAVerificar) {
		for(ProvinciaDTO provincia : this.provincias) {
			if(provincia.getNombre().equals(provinciaAVerificar.getNombre()))
				return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return "PaisDTO [idPais=" + idPais + ", nombre=" + nombre + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idPais;
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		result = prime * result + ((provincias == null) ? 0 : provincias.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaisDTO other = (PaisDTO) obj;
		if (idPais != other.idPais)
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		if (provincias == null) {
			if (other.provincias != null)
				return false;
		} else if (!provincias.equals(other.provincias))
			return false;
		return true;
	}


}
