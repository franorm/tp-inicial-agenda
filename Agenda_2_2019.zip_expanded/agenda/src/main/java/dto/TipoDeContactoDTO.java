package dto;

public class TipoDeContactoDTO {
	private String nombre;
	private int idTipoDeContacto;
	
	public TipoDeContactoDTO(String nombre, int idTipoDeContacto){
		this.setNombre(nombre);
		this.setIdTipoDeContacto(idTipoDeContacto);
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getIdTipoDeContacto() {
		return idTipoDeContacto;
	}
	public void setIdTipoDeContacto(int idTipoDeContacto) {
		this.idTipoDeContacto = idTipoDeContacto;
	}

	@Override
	public String toString() {
		return this.nombre;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idTipoDeContacto;
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoDeContactoDTO other = (TipoDeContactoDTO) obj;
		if (idTipoDeContacto != other.idTipoDeContacto)
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}

}
