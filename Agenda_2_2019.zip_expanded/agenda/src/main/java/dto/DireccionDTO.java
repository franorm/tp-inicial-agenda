package dto;

public class DireccionDTO {
	private String calle, localidad, provincia, departamento;
	private int altura, piso, idDireccion, idPersonaFK, idProvinciaFK;
	
	public DireccionDTO(String calle, String localidad, String provincia, String departamento, int altura, int piso, int idPersonaFK, int idProvinciaFK){
		this.calle = calle;
		this.localidad = localidad;
		this.provincia = provincia;
		this.altura = altura;
		this.piso = piso;
		this.departamento = departamento;
		this.idPersonaFK = idPersonaFK;
		this.idProvinciaFK = idProvinciaFK;
	}
	
	public String getCalle() {
		return calle;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}
	public String getLocalidad() {
		return localidad;
	}
	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}
	public int getAltura() {
		return altura;
	}
	public void setAltura(int altura) {
		this.altura = altura;
	}
	public int getPiso() {
		return piso;
	}
	public void setPiso(int piso) {
		this.piso = piso;
	}
	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	
	public int getIdPersonaFK() {
		return this.idPersonaFK;
	}
	
	public int getIdDireccion() {
		return idDireccion;
	}

	public void setIdDireccion(int idDireccion) {
		this.idDireccion = idDireccion;
	}

	public void setIdPersonaFK(int idPersonaFK) {
		this.idPersonaFK = idPersonaFK;
	}
	
	

	public int getIdProvinciaFK() {
		return idProvinciaFK;
	}

	public void setIdProvinciaFK(int idProvinciaFK) {
		this.idProvinciaFK = idProvinciaFK;
	}

	@Override
	public String toString() {
		return "DireccionDTO [calle=" + calle + ", localidad=" + localidad + ", departamento=" + departamento
				+ ", altura=" + altura + ", piso=" + piso + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + altura;
		result = prime * result + ((calle == null) ? 0 : calle.hashCode());
		result = prime * result + ((departamento == null) ? 0 : departamento.hashCode());
		result = prime * result + ((localidad == null) ? 0 : localidad.hashCode());
		result = prime * result + piso;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DireccionDTO other = (DireccionDTO) obj;
		if (altura != other.altura)
			return false;
		if (calle == null) {
			if (other.calle != null)
				return false;
		} else if (!calle.equals(other.calle))
			return false;
		if (departamento == null) {
			if (other.departamento != null)
				return false;
		} else if (!departamento.equals(other.departamento))
			return false;
		if (localidad == null) {
			if (other.localidad != null)
				return false;
		} else if (!localidad.equals(other.localidad))
			return false;
		if (piso != other.piso)
			return false;
		return true;
	}
	
	
}
