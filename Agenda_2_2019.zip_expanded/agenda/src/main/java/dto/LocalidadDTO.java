package dto;

public class LocalidadDTO {
	private int idLocalidad, idProvincia;
	private String provincia, nombre;
	
	public LocalidadDTO(String nombre, int idLocalidad, String provincia, int idProvincia){
		this.setIdLocalidad(idLocalidad);
		this.setNombre(nombre);
		this.setProvincia(provincia);
		this.setIdProvincia(idProvincia);
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		if(nombre.trim().equals("")) throw new IllegalArgumentException("La localidad debe tener nombre");
		this.nombre = nombre;
	}

	public int getIdLocalidad() {
		return idLocalidad;
	}
	public void setIdLocalidad(int idLocalidad) {
		this.idLocalidad = idLocalidad;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		if(provincia.trim().equals("")) throw new IllegalArgumentException("La localidad debe pertenecer a una provincia");
		this.provincia = provincia;
	}

	public int getIdProvincia() {
		return idProvincia;
	}

	public void setIdProvincia(int idProvincia) {
		this.idProvincia = idProvincia;
	}

	@Override
	public String toString() {
		return "LocalidadDTO [idLocalidad=" + idLocalidad + ", idProvincia=" + idProvincia + ", provincia=" + provincia
				+ ", nombre=" + nombre + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idLocalidad;
		result = prime * result + idProvincia;
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		result = prime * result + ((provincia == null) ? 0 : provincia.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LocalidadDTO other = (LocalidadDTO) obj;
		if (idLocalidad != other.idLocalidad)
			return false;
		if (idProvincia != other.idProvincia)
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		if (provincia == null) {
			if (other.provincia != null)
				return false;
		} else if (!provincia.equals(other.provincia))
			return false;
		return true;
	}

}
