package dto;

import java.util.Date;
import modelo.TipoDeContacto;

public class PersonasPorLocalidadesDTO {
	private int cantidad;
	private String localidad;
	
	public PersonasPorLocalidadesDTO( String localidad ,int cantidad) {
		this.localidad = localidad;
		this.cantidad = cantidad;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}
	
}
