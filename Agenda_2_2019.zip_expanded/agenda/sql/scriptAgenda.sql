CREATE DATABASE `agenda`;
USE agenda;


CREATE TABLE tiposDeContacto(
	idTipoDeContacto integer NOT NULL AUTO_INCREMENT,
	nombre varchar(45),
	CONSTRAINT tipoDeContacto_pk PRIMARY KEY (idTipoDeContacto)
);

CREATE TABLE generosMusicales(
	nombre varchar(45),
	idGeneroMusical int(11) NOT NULL AUTO_INCREMENT,
	CONSTRAINT genero_pk PRIMARY KEY (idGeneroMusical)
);

CREATE TABLE personas
(
  `idPersona` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NULL,
  `Telefono` varchar(45) NULL,
   `Email` varchar(45) NULL,
   `FechaCumpleanios` date NULL,
   `idEtiqueta` integer NULL,
   `idGeneroMusical` int NULL,
  PRIMARY KEY (`idPersona`),
  CONSTRAINT etiqueta_fk FOREIGN KEY (idEtiqueta) REFERENCES tiposDeContacto(idTipoDeContacto) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT gusto_fk FOREIGN KEY (idGeneroMusical) REFERENCES generosMusicales(idGeneroMusical) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE paises(
	nombre varchar(45),
	idPais integer(11) NOT NULL AUTO_INCREMENT,
	CONSTRAINT pais_pk PRIMARY KEY (idPais)
);

CREATE TABLE provincias(
	nombre varchar(45),
	idProvincia integer(11) NOT NULL AUTO_INCREMENT,
	idPaisFK integer(11) NOT NULL,
	CONSTRAINT provincia_pk PRIMARY KEY (idProvincia),
	CONSTRAINT pais_fk FOREIGN KEY (idPaisFK) REFERENCES paises(idPais) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE localidades(
	nombre varchar(45),
	idLocalidad integer(11) NOT NULL AUTO_INCREMENT,
	idProvinciaFK integer(11),
	CONSTRAINT localidad_pk PRIMARY KEY (idLocalidad),
	CONSTRAINT provincia_fk FOREIGN KEY (idProvinciaFK) REFERENCES provincias(idProvincia) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE direcciones(
calle varchar(45),
localidad varchar(45), 
departamento varchar(45),
altura integer,
piso integer,
idDireccion integer NOT NULL AUTO_INCREMENT,
idPersonaFk integer(11),
idProvinciaFK integer(11),
CONSTRAINT direccion_PK PRIMARY KEY (idDireccion),
CONSTRAINT persona_fk FOREIGN KEY (idPersonaFk) REFERENCES personas(idPersona) ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT provincia_fk2 FOREIGN KEY(idProvinciaFK) REFERENCES provincias(idProvincia) ON DELETE CASCADE ON UPDATE CASCADE
);
